var _m = MouseGui(0),
	_x = x, _y = y,
	_w = width, _h = height,
	
	_parent_menu = parent_menu,
	_no_parent = _parent_menu==noone,
	_no_open_child = inside_menu_open==noone,
	_open = open,
	
	_in_name, _in_menu;

//if there is an open inside menu it will handle things
if (_no_parent)
{
	_in_name = point_in_rectangle(_m[0], _m[1], _x, _y, _x+_w, _y+_h);
	_in_menu = point_in_rectangle(_m[0], _m[1], _x, _y+_h, _x+_w, _y+_h+options_ui_height);
}
else
{
	_in_name = point_in_rectangle(_m[0], _m[1], _x+_w, _y+_h, _x+2*_w, _y+2*_h);
	_in_menu = point_in_rectangle(_m[0], _m[1], _x, _y+_h, _x+_w, _y+_h+options_ui_height);
}

if _no_open_child
{
	//close if outside of the menu zones
	if (!_in_name && !_in_menu && _open)
	{
		drop_menu_Close(false); //doing it this way closes the menus one step at a time when outside all of them
	}
	//calculate button focus by mouse pos
	else
	{	
		if (_open && _in_menu)
		{
			#macro MW_SCROLL_SPD _h*10*DeltaTime()
			if mouse_wheel_down() scroll += MW_SCROLL_SPD;
			if mouse_wheel_up() scroll -= MW_SCROLL_SPD;
			scroll = clamp(scroll, 0, max(0, heights_sum*_h - options_ui_height));
			
			var _heights = heights_map;
			var _menu_y = _y+_h;
			var _row = floor((_m[1] - _menu_y + scroll)/_h);
			var _focus = _heights[? _row];
			
			if (_focus != focus)
			{
				focus = _focus;
				redraw_options = true;
			}
		}
	}
}

if (LOGIC.drop_menu_lmb)
{
	//if mouse in the main, base drop down menu button toggle open
	if(_in_name)
	{
		if _open drop_menu_Close(false);
		else if LOGIC.open_menu == _parent_menu
		{
			open = true;
			LOGIC.open_menu = id;
		}
	}
	else
	{
		if (_open && _in_menu)
		{
			var _focus = focus;
			var _opt = options;
			if _focus<ds_list_size(_opt)/4 //might be out of range in case of low number of options
			{
				switch _opt[| _focus*4]
				{
					case OPTION.menu:
						var _inside_menu = _opt[| _focus*4 + 2];
						inside_menu_open = _inside_menu;
						var _scroll = scroll;
						with _inside_menu
						{
							open = true; //TODO: fix this so it does open right here, don't depend on instance execution order code
							x = _x - _w;
							y = _y + (_focus*_h) - _scroll; //1 row above the menu option, so the first option will be by its side
						}
						break;
					case OPTION.script:
						with LOGIC 
						{
							ScriptExecute(_opt[| _focus*4+2], _opt[| _focus*4+3]);
						}
						drop_menu_Close(true);
						break;
				}
				LOGIC.drop_menu_lmb = false; //used this steps' mouse click, flag it off
			}	
		}
	}
}