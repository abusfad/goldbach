#macro DDM_ALPHA 1
draw_set_alpha(DDM_ALPHA);

var _x = x, _y = y,
	_w = width, _h = height,
	
	_scroll = scroll,
	_heights_sum = heights_sum;

//if root menu draw root bar
if (parent_menu == noone)
{
	
	draw_set_color(open ? color_name : color_background);
		draw_rectangle(_x, _y, _x+_w, _y+_h, false);
		
	draw_set_color(c_black);
		draw_triangle(_x+_w*1/64, _y+_h*2/8, _x+_w*3/64, _y+_h*6/8, _x+_w*5/64, _y+_h*2/8, false);
	
	draw_set_font(LOGIC.font_gui);
	draw_set_valign(fa_top);
	draw_set_halign(fa_left);
	draw_set_color(open ? color_background : color_name);
		draw_text(_x+_w*7/64, _y+_h/4, name);
	draw_set_color(c_white);
}

if open
{
	var _opt_ui_h = options_ui_height;
	if (!surface_exists(surf_options))
	{
		//var _surface_h = ds_list_empty(options) ? _opt_ui_h : max(_h*_heights_sum, _opt_ui_h);
		var _surface_h = _opt_ui_h+_h;
		surf_options = surface_create(_w, _surface_h);
		//menu_DrawOptionsSurface();
	}
	//if redraw_options
	//{
		menu_DrawOptionsSurface();
		//redraw_options = false;
	//}
	
	//var _draw_h = min(_opt_ui_h, max(1, _heights_sum*_h + 1));
	draw_surface_part(surf_options, 0, _scroll - floor(_scroll/_h)*_h, _w, _opt_ui_h, _x, _y+_h);
	
	if _scroll > 0
	{
		//draw upward triangle
		draw_set_color(c_black);
		draw_set_alpha(0.5);
			draw_triangle(_x+_w*63/64, _y+_h*14/8, _x+_w*61/64, _y+_h*10/8, _x+_w*59/64, _y+_h*14/8, false);
		draw_set_alpha(1);
		draw_set_color(c_white);
	}
	if (_scroll < _heights_sum*_h - _opt_ui_h)
	{
		//draw downward triangle
		draw_set_color(c_black);
		draw_set_alpha(0.5);
			draw_triangle(_x+_w*63/64, _y+_opt_ui_h+_h*2/8, _x+_w*61/64, _y+_opt_ui_h+_h*6/8, _x+_w*59/64, _y+_opt_ui_h+_h*2/8, false);
		draw_set_alpha(1);
		draw_set_color(c_white);
	}
}

draw_set_color(c_white);
draw_set_alpha(1);
draw_set_valign(fa_middle);
draw_set_halign(fa_center);