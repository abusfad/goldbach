{
    "id": "c669b972-6a55-4f7f-87a3-ab55a6afc237",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_drop_menu",
    "eventList": [
        {
            "id": "48d79f75-80fb-4efd-97f4-d588190a328b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c669b972-6a55-4f7f-87a3-ab55a6afc237"
        },
        {
            "id": "a78b4684-57f5-4a52-bac3-5d72b5db419f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c669b972-6a55-4f7f-87a3-ab55a6afc237"
        },
        {
            "id": "20cc66a2-f626-42ed-9ca3-47c589038b0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c669b972-6a55-4f7f-87a3-ab55a6afc237"
        },
        {
            "id": "148dfecc-3d11-45c6-bae5-31e6c0a399ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "c669b972-6a55-4f7f-87a3-ab55a6afc237"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}