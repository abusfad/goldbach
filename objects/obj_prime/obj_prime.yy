{
    "id": "fe1d0f26-1aaf-4ea9-ae6e-6e3188ce9ea9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_prime",
    "eventList": [
        {
            "id": "a8f85d33-8311-45b9-8885-d407616b5685",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fe1d0f26-1aaf-4ea9-ae6e-6e3188ce9ea9"
        },
        {
            "id": "c3283005-e40a-436d-a7dd-075379e64498",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "fe1d0f26-1aaf-4ea9-ae6e-6e3188ce9ea9"
        },
        {
            "id": "2261504c-a632-4959-bd1e-5265dee3de00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "fe1d0f26-1aaf-4ea9-ae6e-6e3188ce9ea9"
        },
        {
            "id": "729ba9d9-8c44-4b7b-bc02-d4ad05257b99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "fe1d0f26-1aaf-4ea9-ae6e-6e3188ce9ea9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "de9fb7a3-f4e8-4dd1-9790-d318e001973f",
    "visible": true
}