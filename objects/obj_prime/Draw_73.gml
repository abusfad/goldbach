if (active && highlighted)
{
	draw_self();
	
	draw_set_font(font);
	draw_set_valign(fa_center);
	draw_set_halign(fa_center);
	draw_set_color(c_fuchsia);

		draw_text(x, y, string(value));

	draw_set_color(c_white);
}