//chached instances disabing
active = true;

//edges
disabling = list_Create();
disabled_by = list_Create();

//strong components
component = -1;
component_disabling = list_Create();

//loops detection
unblock_connected = list_Create();

//pattern detection utility
blocked = false;

//loops = list_Create();

//visual
highlighted = false;
shown = true;

font = fnt_prime_small;
