if shown
{
	if id!=LOGIC.clicked
	{
		var _id = id;
		with LOGIC
		{
			clicked = _id;
			HighlightPrimeAndItsLists(_id);
		}
	}
	else
	{
		with LOGIC
		{
			clicked = noone;
			UpdateHighlightList(empty_list);
		}
	}
}