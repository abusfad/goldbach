{
    "id": "75e96ff7-5696-47d5-8e70-b2f6ecd16195",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_inside_menu",
    "eventList": [
        {
            "id": "0efc8fc5-32c5-439a-9b57-fda3ebecf7d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "75e96ff7-5696-47d5-8e70-b2f6ecd16195"
        },
        {
            "id": "cbe7079d-0ef8-4316-bc36-aeb7cfb1a786",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "75e96ff7-5696-47d5-8e70-b2f6ecd16195"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bd9afc4d-ff7d-4c43-954b-eb9cf46dad21",
    "visible": true
}