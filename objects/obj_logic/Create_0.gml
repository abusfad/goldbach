#macro LOGIC global.logic
LOGIC = id;

window_set_fullscreen(true);

#region ds managing

free_lists = ds_list_create();
free_maps = ds_list_create();

#endregion

#region instance managing

destroy_on_reset_instances = ds_list_create();

#endregion

#region tests set-up

test_stop = true;
test_value = 0;
test_layer = LAYER.visuals;
test_next_check_index = 0;
//test_result_n_prime = 0;
//test_result_otherwise = 0;

#endregion

#region UI utility

font_gui = fnt_gui;

clicked = noone;

#endregion

InitSystem(38, LAYER.UI_input);