gpu_set_texrepeat(true);

#region not highlighted

shader_set(shd_tracks);
	shader_set_uniform_f(u_time, 3*Time());
	var _alpha = ds_list_empty(highlighted_list) ? 0.7 : 0.6;
	shader_set_uniform_f(u_alpha, _alpha);
	vertex_submit(vbuffer_tracks, pr_trianglelist, sprite_get_texture(spr_direction, 0));
shader_set(shd_tracks_mutual);
	shader_set_uniform_f(u_time_mutual, 3*Time());
	shader_set_uniform_f(u_alpha_mutual, _alpha);
	vertex_submit(vbuffer_tracks_mutual, pr_trianglelist, sprite_get_texture(spr_direction, 0));
shader_reset();

#endregion


#region highlighted

if (!surface_exists(surf_ping))
{
	surf_ping = surface_create(room_width/BLUR_DOWNSCALE, room_height/BLUR_DOWNSCALE);
}
if (!surface_exists(surf_pong))
{
	surf_pong = surface_create(room_width/BLUR_DOWNSCALE, room_height/BLUR_DOWNSCALE);
}
if (!surface_exists(surf_highlighted))
{
	surf_highlighted = surface_create(room_width, room_height);
}
if (!surface_exists(surf_white))
{
	surf_white = surface_create(room_width, room_height);
}

var _glow = glow,
	_s_ping = surf_ping,
	_s_pong = surf_pong,
	_s_highlighted = surf_highlighted,
	_s_white = surf_white,
	_hl = highlighted_list,
	_num = ds_list_size(_hl);

//important for correct blurring
gpu_set_tex_filter(true);
gpu_set_blendmode_ext(bm_one, bm_zero);

surface_set_target(_s_highlighted);
	draw_clear_alpha(c_black, 0);
	shader_set(shd_tracks);
		shader_set_uniform_f(u_time, 3*Time());
		shader_set_uniform_f(u_alpha, 0.8);
		vertex_submit(vbuffer_tracks_highlighted, pr_trianglelist, sprite_get_texture(spr_direction, 0));
	shader_set(shd_tracks_mutual);
		shader_set_uniform_f(u_time_mutual, 3*Time());
		shader_set_uniform_f(u_alpha_mutual, 0.8);
		vertex_submit(vbuffer_tracks_highlighted_mutual, pr_trianglelist, sprite_get_texture(spr_direction, 0));
	shader_reset();
surface_reset_target();

surface_set_target(_s_white);
	draw_clear_alpha(c_black, 0);
	shader_set(shd_white);
		vertex_submit(vbuffer_tracks_highlighted, pr_trianglelist, sprite_get_texture(spr_direction, 0));
		vertex_submit(vbuffer_tracks_highlighted_mutual, pr_trianglelist, sprite_get_texture(spr_direction, 0));
	shader_reset();
	
	for (var i=0; i<_num; i++)
	{
		var _p = _hl[| i];
		with _p draw_self();
	}
surface_reset_target();

surface_set_target(_s_ping);
	draw_clear_alpha(c_black, 0);
	shader_set(shd_blur_axis);
		shader_set_uniform_f(u_blur_vector, 1, 0);
		shader_set_uniform_i(u_blur_length, blur_length);
		shader_set_uniform_f_array(u_blur_kernel, blur_kernel);
		shader_set_uniform_f(u_texel, 1/surface_get_width(_s_ping), 1/surface_get_height(_s_ping));
	
		draw_surface_stretched(_s_white, 0, 0, surface_get_width(_s_ping), surface_get_height(_s_ping));
	shader_reset();
surface_reset_target();

surface_set_target(_s_pong);
	draw_clear_alpha(c_black, 0);
	shader_set(shd_blur_axis);
		shader_set_uniform_f(u_blur_vector, 0, 1);
		shader_set_uniform_i(u_blur_length, blur_length);
		shader_set_uniform_f_array(u_blur_kernel, blur_kernel);
		shader_set_uniform_f(u_texel, 1/surface_get_width(_s_ping), 1/surface_get_height(_s_ping));
	
		draw_surface(_s_ping, 0, 0);
	shader_reset();
surface_reset_target();

gpu_set_blendmode(bm_normal);

shader_set(shd_glow);
	texture_set_stage(smp_glow, surface_get_texture(_s_pong));
	shader_set_uniform_f(u_glow_color, _glow[0], _glow[1], _glow[2], _glow[3]);
	draw_surface(_s_highlighted, 0, 0);
shader_reset();

#endregion

gpu_set_tex_filter(false);
gpu_set_texrepeat(false);
gpu_set_blendmode(bm_normal);