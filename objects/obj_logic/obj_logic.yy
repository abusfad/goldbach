{
    "id": "dd9da2a9-7f4b-4330-b9e1-e4fa24a16c05",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_logic",
    "eventList": [
        {
            "id": "c25ec7f1-af1a-4f95-8856-00b13d1e1666",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dd9da2a9-7f4b-4330-b9e1-e4fa24a16c05"
        },
        {
            "id": "39848e5a-b76c-46fe-94db-0653aa7fcaa7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "dd9da2a9-7f4b-4330-b9e1-e4fa24a16c05"
        },
        {
            "id": "8d28e240-ddc6-4504-9236-e8602164ae19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "dd9da2a9-7f4b-4330-b9e1-e4fa24a16c05"
        },
        {
            "id": "177eac21-61d9-4de4-953e-aa99a3f75795",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "dd9da2a9-7f4b-4330-b9e1-e4fa24a16c05"
        },
        {
            "id": "34c43367-a31d-4e36-ba86-3a9298fccd66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dd9da2a9-7f4b-4330-b9e1-e4fa24a16c05"
        },
        {
            "id": "31648dc7-d004-4fe4-b7cb-9e4a52de85dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dd9da2a9-7f4b-4330-b9e1-e4fa24a16c05"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}