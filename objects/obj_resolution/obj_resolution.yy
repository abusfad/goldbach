{
    "id": "4bb1bbd1-10e6-4a69-bdcb-22d17eb6266a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_resolution",
    "eventList": [
        {
            "id": "ca94d740-e3e4-44e3-a74a-bd05396c5c75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4bb1bbd1-10e6-4a69-bdcb-22d17eb6266a"
        },
        {
            "id": "ccfe2353-1806-4f4c-8ef5-f92ea8fae6f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4bb1bbd1-10e6-4a69-bdcb-22d17eb6266a"
        },
        {
            "id": "1ba3868e-c915-4642-822b-8bfadd3eef2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "4bb1bbd1-10e6-4a69-bdcb-22d17eb6266a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}