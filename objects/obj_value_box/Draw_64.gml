#macro VALUE_BOX_ALPHA 0.6
var _w = display_get_gui_width(),
	_h = display_get_gui_height();

//draw_sprite_stretched_ext(sprite_index, image_index, x*_w, y*_h, width*_w, height*_h, c_white, VALUE_BOX_ALPHA);
draw_set_color(c_gray);
	draw_rectangle(x*_w, y*_h, (x+width)*_w, (y+height)*_h, false);

draw_set_color(c_white);
	draw_rectangle((x+width*1/5)*_w, (y+height/2)*_h, (x+width*4/5)*_w, (y+height*9/10)*_h, false);
	
draw_set_font(LOGIC.font_gui);
draw_set_valign(fa_center);
draw_set_halign(fa_center);
draw_set_color(c_black);
	draw_text((x+width/2)*_w, (y+height*1/4)*_h, name);
//draw_set_color(c_ltgray);
draw_set_color(color_input);
	draw_text((x+width/2)*_w, (y+height*3/4)*_h, user_input);
draw_set_color(c_white);