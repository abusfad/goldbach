{
    "id": "5f489db1-126d-4e69-b5c2-318b5bbaa862",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_value_box",
    "eventList": [
        {
            "id": "05099988-ad7d-4a1f-b3aa-6f1e8f284ddb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "5f489db1-126d-4e69-b5c2-318b5bbaa862"
        },
        {
            "id": "db7a4326-c24f-4865-a219-3419fdc4232a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "5f489db1-126d-4e69-b5c2-318b5bbaa862"
        },
        {
            "id": "725947cc-4801-4613-a356-607a6c96fb22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5f489db1-126d-4e69-b5c2-318b5bbaa862"
        },
        {
            "id": "c81eed3e-8a31-44bf-ab52-48909af5604c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "5f489db1-126d-4e69-b5c2-318b5bbaa862"
        },
        {
            "id": "eb0304e4-0cd7-4137-b277-fc964d8aca2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "5f489db1-126d-4e69-b5c2-318b5bbaa862"
        },
        {
            "id": "ad3154fc-74db-4201-974c-9d26763b32de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "5f489db1-126d-4e69-b5c2-318b5bbaa862"
        },
        {
            "id": "c33e6f17-7d9d-4f6c-a63f-9585430603bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5f489db1-126d-4e69-b5c2-318b5bbaa862"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}