{
    "id": "3f8dcd6c-b26c-4f3f-a346-3f5a2ea6d36a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_direction",
    "For3D": true,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": true,
    "frames": [
        {
            "id": "ffa1fccf-8638-4f17-b48a-13e0b9553d52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f8dcd6c-b26c-4f3f-a346-3f5a2ea6d36a",
            "compositeImage": {
                "id": "2b8234d3-745c-4b1e-99e1-cf60003e000b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffa1fccf-8638-4f17-b48a-13e0b9553d52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b71c26e-1cfd-433b-8767-cd3475d970aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffa1fccf-8638-4f17-b48a-13e0b9553d52",
                    "LayerId": "b140f357-3e8a-4157-b2de-38053065ad43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "b140f357-3e8a-4157-b2de-38053065ad43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f8dcd6c-b26c-4f3f-a346-3f5a2ea6d36a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}