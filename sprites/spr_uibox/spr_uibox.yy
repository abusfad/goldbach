{
    "id": "bd9afc4d-ff7d-4c43-954b-eb9cf46dad21",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_uibox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c199821b-8390-41d8-8ce5-9da2d1fbf151",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd9afc4d-ff7d-4c43-954b-eb9cf46dad21",
            "compositeImage": {
                "id": "b1c3ce83-0837-482d-85de-de80f40091c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c199821b-8390-41d8-8ce5-9da2d1fbf151",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bec5215d-520b-4436-bbcb-177115592a75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c199821b-8390-41d8-8ce5-9da2d1fbf151",
                    "LayerId": "8fe527ed-c51e-43e3-bc0b-bb0873f02ea6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8fe527ed-c51e-43e3-bc0b-bb0873f02ea6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd9afc4d-ff7d-4c43-954b-eb9cf46dad21",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}