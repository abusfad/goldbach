{
    "id": "de9fb7a3-f4e8-4dd1-9790-d318e001973f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prime",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "273429b7-74d1-45ee-a05b-d6e933f3307c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de9fb7a3-f4e8-4dd1-9790-d318e001973f",
            "compositeImage": {
                "id": "008bcb83-6e09-4dc9-8b8c-eded5202cef4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "273429b7-74d1-45ee-a05b-d6e933f3307c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cebb73eb-78b5-4736-a516-feab18b867a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "273429b7-74d1-45ee-a05b-d6e933f3307c",
                    "LayerId": "bb9d424e-2385-42ea-9879-ea51f75c1ec1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bb9d424e-2385-42ea-9879-ea51f75c1ec1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de9fb7a3-f4e8-4dd1-9790-d318e001973f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}