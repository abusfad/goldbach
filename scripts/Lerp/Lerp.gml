///@func Lerp(edge, edge, input)
///@arg edge
///@arg edge
///@arg input
gml_pragma("forceinline");
return lerp(argument0, argument1, argument2);