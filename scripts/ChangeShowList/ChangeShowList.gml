///@func ChangeShowList(shown)
///@arg show

var _to_show = ds_list_duplicate(argument0), //need to duplicate the list so other procedures won't change it
	_shown_prev = show_list;

//ds_list_sort(_to_show, true); //for the adjust room position script

var _num = ds_list_size(_shown_prev);
for (var i=0; i<_num; i++)
{
	var _p = _shown_prev[| i];
	_p.shown = false;
}

//need to free because it was duplicated originally
list_Free(_shown_prev);

var _num = ds_list_size(_to_show);
for (var i=0; i<_num; i++)
{
	var _p = _to_show[| i];
	_p.shown = true;
}

show_list = _to_show;