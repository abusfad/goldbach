///@func HighlightPrimeAndItsLists(prime)
///@arg prime

var _vbuff_hl_base = vbuffer_tracks_highlighted,
	_vbuff_hl_mutual = vbuffer_tracks_highlighted_mutual,
	_format = primes_tracks_vformat,
	
	_p = argument0,
	_disabling = _p.disabling,
	_num_out = ds_list_size(_disabling),
	_disabled_by = _p.disabled_by,
	_num_in = ds_list_size(_disabled_by);

UpdateHighlightList(empty_list); //needed to move everything to the unhighlighted vbuffer first
InputPrimeAndItsLists(ChangeHighlightList, _p, false);

vertex_begin(_vbuff_hl_base, _format);
vertex_begin(_vbuff_hl_mutual, _format);

_p.highlighted = true;

if _p.shown
{
	for (var i=0; i<_num_out; i++)
	{
		var _ptarget = _disabling[| i];
	
		if _ptarget.shown
		{
			var _vbuff = ds_list_find_index(_ptarget.disabling, _p) != -1
			?	_vbuff_hl_mutual
			:	_vbuff_hl_base;
	
			FillTracksVBuffer(_vbuff, [_p.x, _p.y], [_ptarget.x, _ptarget.y]);
		}
	
		_ptarget.highlighted = true;
	}
	for (var i=0; i<_num_in; i++)
	{
		var _pstart = _disabled_by[| i];
	
		if _pstart.shown
		{
			var _vbuff = ds_list_find_index(_disabling, _pstart) != -1
			?	_vbuff_hl_mutual
			:	_vbuff_hl_base;
	
			FillTracksVBuffer(_vbuff, [_pstart.x, _pstart.y], [_p.x, _p.y]);
		}
	
		_pstart.highlighted = true;
	}
}

vertex_end(_vbuff_hl_base);
vertex_end(_vbuff_hl_mutual);