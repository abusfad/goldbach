///@func GoldbachAboveHalfWhenNPrime()

var _floor = ceil(n/2),
	_pil = primes_instances,
	_num = ds_list_size(_pil);

var _last_p = _pil[| _num-1];
if (_last_p.value==n)
{
	var i = test_value;
	do
	{
		var _p = _pil[| i++];
	} until (_p.value >= _floor)
	i--;
	test_value = i;

	for (var j = i; j<_num-1; j++) //lol gml won't let reuse i
	{
		var _p = _pil[| j];
	
		if (ds_list_empty(_p.disabled_by))
		{
			return true;
		}
	}
}

return false;