///@func InitMenus()

show_menu = drop_menu_Init(0.8, 0.05, "Show");
	drop_menu_AddOption(show_menu, "Everything", UpdateShowList, [primes_instances]);
	drop_menu_AddOption(show_menu, "All Components", InputNestedListsSorted, [UpdateShowList, strong_components]);
	
highlight_menu = drop_menu_Init(0.8, 0.15, "Highlight");
	drop_menu_AddOption(highlight_menu, "Nothing", UpdateHighlightList, [empty_list]);
	drop_menu_AddOption(highlight_menu, "All Components", InputNestedListsSorted, [UpdateHighlightList, strong_components]);
	
test_menu = drop_menu_Init(0.8, 0.25, "Data Tests");
	drop_menu_AddOption(test_menu, "Find next component that starts a chain (isn't pointed to from outside)",
TestStart, [LAYER.visuals]);
	drop_menu_AddOption(test_menu, "Same test, without computing the visuals while searching",
TestStart, [LAYER.components]);
	drop_menu_AddOption(test_menu, "Stop test", TestStop, []);

highlight_component_menu = drop_menu_AddMenu(highlight_menu, "Component", false);
highlight_loop_menu = drop_menu_AddMenu(highlight_menu, "Loop", false);
highlight_prime_menu = drop_menu_AddMenu(highlight_menu, "Prime Neighbors", false);

show_component_menu = drop_menu_AddMenu(show_menu, "Component", false);
show_loop_menu = drop_menu_AddMenu(show_menu, "Loop", false);
show_prime_menu = drop_menu_AddMenu(show_menu, "Prime Neighbors", false);

ConfigDropMenusButtonsHeights();

drop_menu_lmb = false;
open_menu = noone;

n_user_box = value_box_Init(0.1, 0.1, "input 2n:", 2*n, GraphRequirement);
LinkOutput(n_user_box, id, OUTPUT.output_script, UpdateSystem, [LAYER.UI_input]);