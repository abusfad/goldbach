///@func SetPrimesVisualData()

var _n = n;

with obj_prime
{
	image_blend = ((2*_n) mod value == 0) ? c_purple : c_white;
	visible = shown;
}