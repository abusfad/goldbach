///@func ComponentStartsChain(component)
///@arg component
gml_pragma("forceinline");

var _c = argument0,
	_num = ds_list_size(_c);

for (var i =0; i<_num; i++)
{
	if PrimePointedFromOutsideComponent(_c[| i], _c)
	{
		return false;
	}
}
return true;