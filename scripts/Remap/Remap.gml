///@func Remap(input, low_old, high_old, low_new, high_new)
///@arg input
///@arg low_old
///@arg high_old
///@arg low_new
///@arg high_new

return argument1+(argument4-argument3)*((argument0-argument1)/(argument2-argument1));