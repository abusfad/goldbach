///@func AdjustPrimesRoomLocation(distribution)
///@arg distribution

enum DISTRIBUTION
{
	even,
	linear,
	log
}

var _center = [room_width/2, room_height/2];
var _radius = min(room_width/2, room_height/2);
_radius -= sprite_get_xoffset(spr_prime); //circle shaped sprite

var _p_list = show_list,
	_num = ds_list_size(_p_list);

if !ds_list_empty(_p_list)
{
	var	_highest_prime = _p_list[| _num-1], //only works if the primes list is already sorted
		_highest_val = _highest_prime.value,
		_n = n;
	for (var i=0; i<_num; i++)
	{
		var _p = _p_list[| i];
		switch distribution
		{
			case DISTRIBUTION.even:
				var _angle = (i/_num) * 360;
				break;
			case DISTRIBUTION.linear:
				var _angle = (_p.value/_n) * 360;
				break;
			case DISTRIBUTION.log:
				var _angle = power((log2(_p.value)/log2(_highest_val)), 2) * 360;
				break;
		}
		_p.x = _center[0] + lengthdir_x(_radius, _angle+90); //90 because in GM angle 0 is right and counter-clockwise
		_p.y = _center[1] + lengthdir_y(_radius, _angle+90);
	}
}