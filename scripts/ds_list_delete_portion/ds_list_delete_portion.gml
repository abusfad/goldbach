///@func ds_list_delete_portion(source, start, end)
///@arg list
///@arg start
///@arg end

var _list = argument0,
	_start = argument1,
	_end = argument2;

repeat(_end-_start + 1)
{
	ds_list_delete(_list, _start);
}