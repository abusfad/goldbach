///@func InputMultipleLists(script, list_1, list2, ...)
///@arg script
///@arg list_1
///@arg list_2
///@arg ...

var _temp = list_Create();

for (var i=1; i<argument_count; i++)
{
	ds_list_append(_temp, argument[i]);
}

script_execute(argument[0], _temp);

list_Free(_temp);