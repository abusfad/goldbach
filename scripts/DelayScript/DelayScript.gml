///@func scr_delay_script(id, steps, script, arguments_array)
///@arg id instance to run the code
///@arg steps number of steps to delay the code
///@arg script
///@arg arguments_array
///scripts will run at the start of the begin_step, at the end of obj_logic's event. So no draw functions.

with (LOGIC) ds_list_add(delayed_scripts, [argument0, argument1, argument2, argument3]);

enum delayed
{
	id,
	steps,
	script,
	args
}