///@func PopulatePrimeLists()

var _pil = primes_instances,
	_num = ds_list_size(_pil);

for (var i=0; i<_num; i++)
{
	PopulateDisableLists(_pil[| i]);
}