///@func LoopUnblock(prime)
///@arg prime

var _p = argument0,
	_unblock = _p.unblock_connected;

_p.blocked = false;

while (!ds_list_empty(_unblock))
{
	var _p_next = _unblock[| 0];
	ds_list_delete(_unblock, 0);
	LoopUnblock(_p_next);
}