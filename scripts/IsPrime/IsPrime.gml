///@func IsPrime(num)
///@arg num
gml_pragma("forceinline");

var _primes = primes,
	_size = ds_list_size(_primes),
	_num = argument0;

var i = 0;
for (var i = 0; i<_size; i++)
{
	if (_num mod _primes[| i] == 0) return false;
}

return true;