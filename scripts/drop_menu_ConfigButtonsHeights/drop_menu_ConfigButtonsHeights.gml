///@func drop_menu_ConfigButtonsHeights(menu)
///@arg menu

with argument0
{
	var _opt = options,
		_num = ds_list_size(_opt)/4,
	
		_list = options_heights,
		_map = heights_map,
	
		_w = width;

	ds_map_clear(_map);
	ds_list_clear(_list);

	draw_set_font(LOGIC.font_gui);

	var i = 0, row = 0;
	repeat(_num)
	{		
		var _name = _opt[| i*4 + 1],
			_row_height = ceil(string_width(_name) / _w);
		
		ds_list_add(_list, _row_height);
		repeat _row_height
		{
			_map[? row++] = i;
		}
		i++;
	}
}