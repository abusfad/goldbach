///@func MouseGuiPercent(device)
///@arg device
return	[	device_mouse_x_to_gui(argument0) / display_get_gui_width(),
			device_mouse_y_to_gui(argument0) / display_get_gui_height()
		];