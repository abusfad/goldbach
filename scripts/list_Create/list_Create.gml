///@func list_Create()
gml_pragma("forceinline");

var _free = LOGIC.free_lists;

if ds_list_empty(_free)
{
	return ds_list_create();
}
else
{
	var _last = ds_list_size(_free)-1;
	var _list = _free[| _last];
	ds_list_delete(_free, _last);
	return _list;
}