///@func BuildListShowVBuffer(list)
///@arg list

//first return everything to the base vbuffer.

//for the system show functionality don't input the loop list with the repeated first and last prime
var _unrepeating_list = ds_list_duplicate(argument0);
ds_list_delete(_unrepeating_list, ds_list_size(_unrepeating_list)-1);

UpdateShowList(_unrepeating_list);

list_Free(_unrepeating_list);

BuildListHighlightedVBuffer(argument0);
//var _list = argument0,
//	_num = ds_list_size(_list);

//list_Free(show_list);
//show_list = _unrepeating_list;

//for (var i=0; i<_num; i++)
//{
//	var _p = _list[| i];
//	_p.shown = true;
//}

//UpdateSystemVisuals();

//UpdatePrimesVisualVars();
//AdjustPrimesRoomLocation();

//var _vbuff_base = vbuffer_tracks,
//	_vbuff_mutual = vbuffer_tracks_mutual,
//	_format = primes_tracks_vformat;
	
//vertex_begin(_vbuff_base, _format);
//vertex_begin(_vbuff_mutual, _format);

//for (var i=0; i<_num-1; i++)
//{
//	var _pstart = _list[| i],
//		_ptarget = _list[| i+1];
	
//	var _vbuff = ds_list_find_index(_ptarget.disabling, _pstart) != -1
//	?	_vbuff_mutual
//	:	_vbuff_base;
	
//	FillTracksVBuffer(_vbuff, [_pstart.x, _pstart.y], [_ptarget.x, _ptarget.y]);
//}

//vertex_end(_vbuff_base);
//vertex_end(_vbuff_mutual);