///@func HandleNumberInput(string)
///@arg string

var _input = argument0;

_input += string_digits(keyboard_lastchar);
	
if (keyboard_lastchar=="-")
{
	_input = (string_char_at(_input, 1)=="-")
	?	string_delete(_input, 1, 1)
	:	string_insert("-", _input, 1);
}
	
if (keyboard_lastchar=="+")
{
	if (string_char_at(_input, 1)=="-") _input = string_delete(_input, 1, 1);
}
	
if (keyboard_lastchar==".")
{
	if (string_count(".", _input)<=0) _input += ".";
}

//clear the lastchar variable so it won't repeat itself incase of a keypress that doesn't update it
keyboard_lastchar = "";

if (keyboard_check_pressed(vk_backspace))
{
	return string_delete(_input, string_length(_input), 1);
}

return _input;