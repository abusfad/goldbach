///@func ConstructComponentsDisablingLists()

var _sc = strong_components,
	_num = ds_list_size(_sc);
	//_sc_map = strong_components_map;

for (var i=0; i<_num; i++)
{
	var _component = _sc[| i],
		_size = ds_list_size(_component),
		//_map = _sc_map[? _component];
	for (var j=0; j<_size; j++)
	{
		var _p = _component[| j],
			_dis = _p.disabling,
			_length = ds_list_size(_dis),
			_c_dis = _p.component_disabling;
		for (var k=0; k<_length; k++)
		{
			var _p_next = _dis[| k];
			if (_p_next.component == _component)
			{
				ds_list_add(_c_dis, _p_next);
			}
		}
	}
}