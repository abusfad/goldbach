///@func PrimesListToString(primes_list)
///@arg primes_list

var _string = "",
	_list = argument0,
	_num_1 = ds_list_size(_list) - 1;

for (var i=0; i<_num_1; i++)
{
	var _p = _list[| i];
	_string += string(_p.value) + ", ";
}
var _p = _list[| _num_1];
_string += string(_p.value);

return _string;