///@func LinkOutput(source, linked_instance, output_type, var_name_or_script_id, optional_index_or_args)
///@arg source
///@arg linked_instance
///@arg output_type
///@arg var_name_or_script_id
///@arg optional_index_or_args

var _pusher = argument[0];

ds_list_add(_pusher.linked_outputs, argument[1]);

ds_list_add(_pusher.linked_outputs, argument[2]);

ds_list_add(_pusher.linked_outputs, argument[3]);

if (argument_count==5)
{
	ds_list_add(_pusher.linked_outputs, argument[4]);
}
else
{
	ds_list_add(_pusher.linked_outputs, undefined);
}

with _pusher PushOutput();