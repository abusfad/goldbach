///@func BuildPrimesListVBuffer(vbuffer, list)
///@arg vbuffer
///@arg list

var _vbuff = argument0,
	_list = argument1,
	_num = ds_list_size(_list),
	
	_format = primes_tracks_vformat;

vertex_begin(_vbuff, _format);

for (var i=0; i<_num-1; i++)
{
	var _pstart = _list[| i],
		_ptarget = _list[| i+1];
		
	FillTracksVBuffer(_vbuff, [_pstart.x, _pstart.y], [_ptarget.x, _ptarget.y]);
}