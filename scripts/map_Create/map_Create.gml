///@func map_Create()
gml_pragma("forceinline");

var _free = LOGIC.free_maps;

if ds_list_empty(_free)
{
	return ds_map_create();
}
else
{
	var _last = ds_list_size(_free)-1;
	var _map = _free[| _last];
	ds_list_delete(_free, _last);
	return _map;
}