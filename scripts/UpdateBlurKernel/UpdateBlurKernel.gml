///@func shader_UpdateBlurKernel()

//leaves extra kernel entries untouched, will not be used in the shade due to the blur_kernel_length uniform
var _kernel = blur_kernel,
	_length = blur_length,
	_s = blur_sigma;

var i = 0;
repeat(_length+1)
{
	_kernel[@ i] = exp(-sqr(i)/(2*sqr(_s)));
	i++;
}