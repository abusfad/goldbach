///@func GoldbachBelowHalf()


var _ceil = floor(n/2),
	_pil = primes_instances,
	_num = ds_list_size(_pil);

for (var i=0; i<_num; i++)
{
	var _p = _pil[| i];
	if _p.value>_ceil
	{
		var _p_last = _pil[| _num-1];
		if (_p_last.value==n)
		{
			return 2;
		}
		else
		{
			return 1;
		}
	}
	else if (ds_list_empty(_p.disabled_by))
	{
		return 0;
	}
}