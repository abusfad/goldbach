///@func FillTracksVBuffer(vbuffer, start, end)
///@arg vbuffer
///@arg start
///@arg end

#macro HALF_TRACKS_W 6
#macro TRACKS_H 24

var _vbuff = argument0,
	_pos = argument1,
	_target_pos = argument2,

	_dir = point_direction(_pos[0], _pos[1], _target_pos[0], _target_pos[1]),
			
	_base_corner_left = [_pos[0]+lengthdir_x(HALF_TRACKS_W, _dir+90), _pos[1]+lengthdir_y(HALF_TRACKS_W, _dir+90)],
	_base_corner_right = [_pos[0]+lengthdir_x(HALF_TRACKS_W, _dir-90), _pos[1]+lengthdir_y(HALF_TRACKS_W, _dir-90)],
			
	_target_corner_left = [_target_pos[0]+lengthdir_x(HALF_TRACKS_W, _dir+90), _target_pos[1]+lengthdir_y(HALF_TRACKS_W, _dir+90)],
	_target_corner_right = [_target_pos[0]+lengthdir_x(HALF_TRACKS_W, _dir-90), _target_pos[1]+lengthdir_y(HALF_TRACKS_W, _dir-90)],
			
	_sprite_repeats = point_distance(_pos[0], _pos[1], _target_pos[0], _target_pos[1])/TRACKS_H;
	
vertex_position(_vbuff, _base_corner_left[0], _base_corner_left[1]);
vertex_texcoord(_vbuff, 0, 0);
		
vertex_position(_vbuff, _base_corner_right[0], _base_corner_right[1]);
vertex_texcoord(_vbuff, 0, 1);

vertex_position(_vbuff, _target_corner_left[0], _target_corner_left[1]);
vertex_texcoord(_vbuff, _sprite_repeats, 0);
		
vertex_position(_vbuff, _target_corner_right[0], _target_corner_right[1]);
vertex_texcoord(_vbuff, _sprite_repeats, 1);
		
vertex_position(_vbuff, _target_corner_left[0], _target_corner_left[1]);
vertex_texcoord(_vbuff, _sprite_repeats, 0);
		
vertex_position(_vbuff, _base_corner_right[0], _base_corner_right[1]);
vertex_texcoord(_vbuff, 0, 1);