///@func ForEach(list, script, args_array)
///@arg list
///@arg script
///@arg args_array

var _list = argument0,
	_num = ds_list_size(_list);

for (var i=0; i<_num; i++)
{
	var _args = [_list[| i]];
	array_copy(_args, 1, argument2, 0, array_length_1d(argument2));
	ScriptExecute(argument1, _args)
}