///@func SmoothStepNorm(repeats, input)
///@arg repeats
///@arg input
gml_pragma("forceinline");

if (argument0<=0) return argument1;
else
{
	var _result = argument1 * argument1 * (3 - 2*argument1);
	if (argument0<=1) return _result;
	else return SmoothStepNorm(argument0-1, _result);
}
