///@func drop_menu_Init(x_percent, y_percent, name)
///@arg x_percent
///@arg y_percent
///@arg name


var _ddm = instance_create_layer(argument0*display_get_gui_width(), argument1*display_get_gui_height(), "UI", obj_drop_menu);

with (_ddm)
{	
	options = list_Create();
	options_heights = list_Create();
	heights_map = map_Create();
	
	#macro OPTIONS_UI_HEIGHT 0.25
	options_ui_height = OPTIONS_UI_HEIGHT*display_get_gui_height();
	
	open = false;
	scroll = 0;
	focus = noone;
	inside_menu_open = noone;
	parent_menu = noone;
	
	#macro DDM_WIDTH 0.15
	#macro DDM_HEIGHT 0.025
	width = DDM_WIDTH*display_get_gui_width();
	height = DDM_HEIGHT*display_get_gui_height();
	
	name = argument2;
	color_name = c_teal;
	color_background = c_white;
	
	surf_options = -1;
	redraw_options = false;
}

return _ddm;