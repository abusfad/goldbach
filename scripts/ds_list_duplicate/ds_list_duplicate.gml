///@func ds_list_duplicate(list)
///@arg list

var _list = list_Create();
ds_list_copy(_list, argument0);
return _list;