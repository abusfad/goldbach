///@func StrongComponentsRecursiveStep(prime)
///@arg prime

var _p = argument0,
	_dis = _p.disabling,
	_num = ds_list_size(_dis);

_p.blocked = true;

for (var i=0; i<_num; i++)
{
	var _p_next = _dis[| i];
	if (!_p_next.blocked)
	{
		StrongComponentsRecursiveStep(_p_next);
	}
}

ds_stack_push(order_stack, _p);