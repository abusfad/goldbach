///@func UpdateSystem(even_number, layer_to_compute)
///@arg even_number
///@arg layer_to_compute

if (GraphRequirement(argument0))
{
	n = argument0/2;

	var _layer = argument1;
	enum LAYER
	{
		base,
		components,
		loops,
		visuals,
		UI_input
	}

	ResetVariables();

	AddPrimes();

	AddPrimesInstances();

	PopulatePrimeLists();

	if (_layer>=LAYER.components)
	{
		BuildStrongComponents();
	
		if (_layer>=LAYER.loops)
		{
			BuildLoops();
		
			if (_layer >= LAYER.visuals)
			{
				ResetSystemVisuals();
			
				if (_layer>=LAYER.UI_input)
				{
					UpdateDropMenus();
					UpdateInputBox();
				}
			}
		}
	}
}