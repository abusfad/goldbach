///@func InputPrimeAndItsLists(script, prime, sorted)
///@arg script
///@arg prime
///@arg sorted

var _p = argument1,
	_temp = list_Create();

ds_list_add(_temp, _p);
ds_list_append(_temp, _p.disabling);
ds_list_append(_temp, _p.disabled_by);

if argument2 ds_list_sort(_temp, true);

script_execute(argument0, _temp);

list_Free(_temp);