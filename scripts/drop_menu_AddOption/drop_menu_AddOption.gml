///@func drop_menu_AddOption(menu, option_name, option_script, script_args)
///@arg menu
///@arg option_name
///@arg option_script
///@arg script_args

enum OPTION
{
	script,
	menu
}

ds_list_add(argument0.options, OPTION.script, argument1, argument2, argument3);