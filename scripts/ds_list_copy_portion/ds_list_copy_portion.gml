///@func ds_list_copy_portion(target, source, start, end)
///@arg target
///@arg source
///@arg start
///@arg end

var _tar = argument0,
	_src = argument1;

ds_list_clear(_tar);

var _end = argument3;
for (var i=argument2; i<=_end; i++)
{
	ds_list_add(_tar, _src[| i]);
}