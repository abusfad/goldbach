///@func StringToReal(string)
///@arg string

var _str = argument0,
	_result = 0,
	_sign = 1,

if (string_char_at(_str, 0)=="-")
{
	_sign = -1;
	_str = string_delete(_str, 1, 1); //so the string will fit the loop
}

var _length = string_length(_str);
var _digit, i = 1;
repeat (_length)
{
	var _char = string_char_at(_str, i);
	switch _char
	{
		case "0": _digit = 0; break;
		case "1": _digit = 1; break;
		case "2": _digit = 2; break;
		case "3": _digit = 3; break;
		case "4": _digit = 4; break;
		case "5": _digit = 5; break;
		case "6": _digit = 6; break;
		case "7": _digit = 7; break;
		case "8": _digit = 8; break;
		case "9": _digit = 9; break;
	}
	
	if (_char==".") //can't be in the switch statement because the "break" keyword will switch functionality...
	{
		break;
	}
	
	_result = _result*10 + _digit;
	
	i++;
}

//construct the decimals in reverse order, starting at the end of the string. i is the index of the decimal point
var _decimals = 0;
repeat (_length-i)
{
	var _char = string_char_at(_str, _length--);
	switch _char
	{
		case "0": _digit = 0; break;
		case "1": _digit = 1; break;
		case "2": _digit = 2; break;
		case "3": _digit = 3; break;
		case "4": _digit = 4; break;
		case "5": _digit = 5; break;
		case "6": _digit = 6; break;
		case "7": _digit = 7; break;
		case "8": _digit = 8; break;
		case "9": _digit = 9; break;
	}
	_decimals = (_decimals+_digit)/10;
}

return (_result+_decimals)*_sign;