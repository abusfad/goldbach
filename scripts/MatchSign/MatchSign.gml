///@func MatchSign(input, target)
///@desc MatchSign(input, target)
///@arg input
///@arg target

if (sign(argument0)==sign(argument1) || sign(argument1)==0) return argument0;
else return -argument0;