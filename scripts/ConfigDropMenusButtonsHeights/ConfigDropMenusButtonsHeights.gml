///@func ConfigDropMenusButtonsHeights()

draw_set_font(LOGIC.font_gui);

with obj_drop_menu
{
	var _opt = options,
		_num = ds_list_size(_opt)/4,
	
		_list = options_heights,
		_map = heights_map,
		
		_heights_sum = 0,
		
		_w = width*63/64,
		_h = height;

	ds_map_clear(_map);
	ds_list_clear(_list);

	var i = 0, row = 0;
	repeat(_num)
	{		
		var _name = _opt[| i*4 + 1],
			_row_height = ceil(string_height_ext(_name, _h, _w) / _h);
		
		_heights_sum += _row_height;
		
		ds_list_add(_list, _row_height);
		repeat _row_height
		{
			_map[? row++] = i;
		}
		i++;
	}
	
	heights_sum = _heights_sum;
	
	surface_free(surf_options);
}