///@func SmoothStep(repeats, edge0, edge1, input)
///@arg repeats
///@arg edge0
///@arg edge1
///@arg input

var _whole = argument2 - argument1;
var _t = clamp((argument3 - argument1) / _whole, 0, 1);
var _result = (_t * _t * (3 - 2*_t));
if (argument0<=0) return argument3;
else if (argument0<=1) return argument1+_result*_whole;
else return argument1+SmoothStepNorm(argument0-1, _result)*_whole;