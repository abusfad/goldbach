///@func NestedListAction(list, depth, script, args_array)
///@arg list
///@arg depth
///@arg script
///@arg args_array

var _list = argument0,
	_num = ds_list_size(_list),
	_depth = argument1;

if (_depth==2)
{
	for (var i=0; i<_num; i++)
	{
		ForEach(_list[| i], argument2, argument3);
	}
}
else
{
	for (var i=0; i<_num; i++)
	{
		NestedListAction(_list[| i], _depth-1, argument2, argument3);
	}
}