///@func PopulateDisableLists(prime_instance)
///@arg prime_instance
var _p = argument0,
	_pair = 2*n - _p.value,
	_dbl = _p.disabled_by,
	
	_pil = primes_instances,
	_size = ds_list_size(_pil);

for (var i = 0; i<_size; i++)
{
	var _disabler = _pil[| i];
	if (_pair mod _disabler.value == 0)
	{
		ds_list_add(_dbl, _disabler);
		ds_list_add(_disabler.disabling, _p);
	}
}