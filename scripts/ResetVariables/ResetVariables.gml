///@func ResetVariables()

ClearNestingList(strong_components, 2);
ClearNestingList(loops, 2);
ds_stack_clear(order_stack);

test_next_check_index = 0;

with obj_prime
{
	ds_list_clear(component_disabling);
	ds_list_clear(disabling);
	ds_list_clear(disabled_by);
	ds_list_clear(unblock_connected);
	
	active = true;
	blocked = false;
	component = -1;
}