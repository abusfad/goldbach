///@func value_box_UpdateOutput()

//don't leave redundant 0's at the start of the string
//var _first_digit = (string_char_at(user_input, 1)=="-") ? 2 : 1;
//while (string_char_at(user_input, _first_digit)=="0")
//{
//	user_input = string_delete(user_input, _first_digit, 1);
//}

var _input = StringToReal(user_input);

if script_execute(input_test, _input)
{
	output = _input;
	user_input = string(_input);
	
	PushOutput();
}
else
{
	user_input = string(output);
}