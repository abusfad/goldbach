///@func ds_list_append(target_list, list_input)
///@arg target_list
///@arg list_input

var i = 0;
repeat(ds_list_size(argument1))
{
	ds_list_add(argument0, argument1[| i++]);
}