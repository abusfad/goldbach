///@func UpdateDropMenus()

var _hcm = highlight_component_menu,
	_hlm = highlight_loop_menu,
	_hpm = highlight_prime_menu,
	
	_scm = show_component_menu,
	_slm = show_loop_menu,
	_spm = show_prime_menu;

ds_list_clear(_hcm.options);
ds_list_clear(_hlm.options);
ds_list_clear(_hpm.options);
ds_list_clear(_scm.options);
ds_list_clear(_slm.options);
ds_list_clear(_spm.options);

ClearInstances();

with obj_drop_menu
{
	drop_menu_Close(true);
}

var _sc = strong_components,
	_num = ds_list_size(_sc);
for (var i=0; i<_num; i++)
{
	drop_menu_AddOption(_hcm, PrimesListToString(_sc[| i]), UpdateHighlightList, [_sc[| i]]);
	drop_menu_AddOption(_scm, PrimesListToString(_sc[| i]), UpdateShowList, [_sc[| i]]);
}

var _loops = loops,
	_num = ds_list_size(_loops);
for (var i=0; i<_num; i++)
{
	//BuildPrimesListVBuffer is only accurate while the loop list is in the loop's order. also it's not really dynamic to any vbuffer /:
	drop_menu_AddOption(_hlm, PrimesListToString(_loops[| i]), BuildListHighlightedVBuffer, [_loops[| i]]);
	drop_menu_AddOption(_slm, PrimesListToString(_loops[| i]), BuildListShowVBuffer, [_loops[| i]]);
}

var _pil = primes_instances,
	_num = ds_list_size(_pil);
for (var i=0; i<_num; i++)
{
	var _p = _pil[| i];
	
	drop_menu_AddOption(_hpm, string(_p.value), HighlightPrimeAndItsLists, [_p]);
	drop_menu_AddOption(_spm, string(_p.value), InputPrimeAndItsLists, [UpdateShowList, _p, true]);
}

ConfigDropMenusButtonsHeights();