///@func BuildStrongComponents()

var _sc = strong_components,
	_stack = order_stack,
	//_sc_map = strong_components_map,
	_pil = primes_instances,
	_num = ds_list_size(_pil);

for (var i=0; i<_num; i++)
{
	var _p = _pil[| i];
	if (!_p.blocked)
	{
		StrongComponentsRecursiveStep(_p);
	}
}

obj_prime.blocked = false; //reset for 2nd phase

var _flag = false;

while (!ds_stack_empty(_stack))
{
	var _p = ds_stack_pop(_stack);
	if (!_p.blocked)
	{
		if (!_flag)
		{
			var _component = list_Create();
		}
		
		StrongComponentSecondPhase(_p, _component);
		
		if (ds_list_size(_component)>1)
		{	
			_flag = false;
			
			ds_list_sort(_component, true);
			ds_list_add(_sc, _component);
			
			for (var i=0; i<ds_list_size(_component); i++)
			{
				var _prime = _component[| i];
				_prime.component = _component;
				_prime.component_index = i;
			}
		}
		else
		{
			//don't add to components, instead use the same list when next needed
			_flag = true;
			ds_list_clear(_component);
		}
	}
}
//prevent memory leak if last list was unused
if (_flag = true) list_Free(_component);

ConstructComponentsDisablingLists();

//OutputNestedPrimesListToFile(_sc, "strong components.txt");