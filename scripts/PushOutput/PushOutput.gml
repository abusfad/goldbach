///@func PushOutput()
var _linked = linked_outputs,
	_num = ds_list_size(_linked),
	_output = output;

enum OUTPUT
{
	var_name,
	output_script,
	script,
	array,
	list,
	map
}

var i = 0;
repeat (_num/4)
{
	var _inst = _linked[| i];
	switch (_linked[| i+1])
	{
		case OUTPUT.output_script:
			with (_inst)
			{
				var _args = [_output];
				array_copy(_args, 1, _linked[| i+3], 0, array_length_1d(_linked[| i+3]));
				
				ScriptExecute(_linked[| i+2], _args);
			}
			break;
		case OUTPUT.script:
			with (_inst)
			{	
				ScriptExecute(_linked[| i+2], _linked[| i+3]);
			}
			break;
		case OUTPUT.var_name:
			variable_instance_set(_inst, _linked[| i+2], _output);
			break;
		case OUTPUT.array:
			var _arr = _linked[| i+2];
			_arr[@ _linked[| i+3]] = _output;
			break;
		case OUTPUT.list:
			var _list = _linked[| i+2];
			_list[| _linked[| i+3]] = _output;
			break;
		case OUTPUT.map:
			var _map = _linked[| i+2];
			_map[? _linked[| i+3]] = _output;
			break;
	}
	i += 4;
}