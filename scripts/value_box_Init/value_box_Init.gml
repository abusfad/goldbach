///@func value_box_Init(x, y, name, init_val, input_test)
///@arg x_percent
///@arg y_percent
///@arg name
///@arg init_val
///@arg input_test

var _vb = instance_create_layer(argument0, argument1, "UI", obj_value_box);
with _vb
{

	name = argument2;
	color_name = c_ltgray;
	color_input = c_black;
	linked_outputs = ds_list_create();

	#macro VB_GUI_WIDTH 0.05
	#macro VB_GUI_HEIGHT 0.05
	
	width = VB_GUI_WIDTH
	height = VB_GUI_HEIGHT

	output = argument3;
	user_input = string(argument3);
	input_test = argument4;
	typing = false;
}

return _vb;