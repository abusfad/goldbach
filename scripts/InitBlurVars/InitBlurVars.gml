blur_kernel = [];
blur_length = 6;
blur_sigma = 12;
UpdateBlurKernel();

#macro BLUR_DOWNSCALE 2

u_blur_vector = shader_get_uniform(shd_blur_axis, "u_blur_vector"); //should be (1,0) for horizontal blur or (0,1) for vertical
u_blur_kernel = shader_get_uniform(shd_blur_axis, "u_blur_kernel");
u_blur_length = shader_get_uniform(shd_blur_axis, "u_blur_length");
u_texel = shader_get_uniform(shd_blur_axis, "u_texel");