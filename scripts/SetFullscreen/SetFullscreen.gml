///@func SetFullscreen

if(argument0)
{
	window_set_fullscreen(true);
	//display_set_gui_size(display_get_width(), display_get_height());
	gui_font = fnt_gui_full;	
}
else
{
	window_set_fullscreen(false);
	display_set_gui_maximize(-1, -1);
	//display_set_gui_size(640, 360);
	gui_font = fnt_gui_windowed;
}

DelayScript(LOGIC, 2, UpdateDisplayData, []);