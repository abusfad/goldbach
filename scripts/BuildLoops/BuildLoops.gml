///@func BuildLoops()

var _sc = strong_components,
	_num = ds_list_size(_sc);

for (var i=0; i<_num; i++)
{
	var _component = _sc[| i],
		_size = ds_list_size(_component);
	for (var j=0; j<_size; j++)
	{
		for (var k=0; k<_size; k++)
		{
			LoopUnblock(_component[| k]);
		}
		RecursiveLoopStep(j, _component[| j]);
	}
}


//write loops in file for reflection
//OutputNestedPrimesListToFile(loops, "loops.txt");