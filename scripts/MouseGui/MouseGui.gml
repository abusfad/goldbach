///@func MouseGui(device)
///@arg device
return	[	device_mouse_x_to_gui(argument0),
			device_mouse_y_to_gui(argument0)
		];