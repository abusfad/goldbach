///@func InterpolateArray(dest, edge, edge, interpolation_script, input)
///@arg dest
///@arg edge
///@arg edge
///@arg interpolation_script
///@arg input
gml_pragma("forceinline");

var _dest = argument0,
	_e1 = argument1,
	_e2 = argument2,
	_script = argument3,
	_input = argument4,
	
	_len = array_length_1d(_dest);

for (var i=0; i<_len; i++)
{
	_dest[@ i] = script_execute(_script, _e1[i], _e2[i], _input);
}