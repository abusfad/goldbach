///@func RecursiveLoopStep(starting_index, current_index)
///@arg starting_index
///@arg current_prime

var _lcs = loop_current_steps,
	_start = argument0,
	_p = argument1,
	_found = false,
	
	_dis = _p.component_disabling,
	_num = ds_list_size(_dis);

_p.blocked = true;
ds_list_add(_lcs, _p);

for (var i=0; i<_num; i++)
{
	var _p_next = _dis[| i];
	if (_p_next.component_index < _start)
	{
		continue;
	}
	else if (_p_next.component_index == _start)
	{
		_found = true;
		var _loop = ds_list_duplicate(_lcs);
		ds_list_add(_loop, _p_next); //adding the first prime again
		ds_list_add(loops, _loop);
	}
	else if (!_p_next.blocked)
	{
		var _gml = RecursiveLoopStep(_start, _p_next);
		_found = (_found || _gml);
	}
}

if (_found)
{
	LoopUnblock(_p);
}
else
{
	for (var i=0; i<_num; i++)
	{
		var _p_next = _dis[| i];
		ds_list_add(_p_next.unblock_connected, _p);
	}
}

ds_list_delete(_lcs, ds_list_size(_lcs)-1); //done with this one, remove from current steps list

return _found;