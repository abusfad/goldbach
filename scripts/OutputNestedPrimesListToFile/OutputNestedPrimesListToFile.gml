///@func OutputNestedPrimesListToFile(list, file_name)
///@arg list
///@arg file_name

//write loops in file for reflection
var _string = "",
	_list = argument0,
	_num = ds_list_size(_list);

for (var i=0; i<_num; i++)
{
	var _item = _list[| i];
	_string += PrimesListToString(_item) + "\n";
}
var _file = file_text_open_write(working_directory+argument1);
file_text_write_string(_file, _string);
file_text_close(_file);