///@func StrongComponentSecondPhase(prime, component_list)
///@arg prime
///@arg component_list
var _p = argument0,
	_component = argument1,
	
	_dis = _p.disabled_by, //graph advancement reversed according to the algorithm
	_num = ds_list_size(_dis);

ds_list_add(_component, _p)

_p.blocked = true;

for (var i=0; i<_num; i++)
{
	var _p_next = _dis[| i];
	if (!_p_next.blocked)
	{
		StrongComponentSecondPhase(_p_next, _component);
	}
}