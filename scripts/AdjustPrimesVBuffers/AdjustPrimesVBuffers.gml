///@func AdjustPrimesVBuffers()

vertex_begin(vbuffer_tracks, primes_tracks_vformat);
vertex_begin(vbuffer_tracks_mutual, primes_tracks_vformat);
vertex_begin(vbuffer_tracks_highlighted, primes_tracks_vformat);
vertex_begin(vbuffer_tracks_highlighted_mutual, primes_tracks_vformat);

var _p_list = primes_instances,
	_num = ds_list_size(_p_list);
for (var i=0; i<_num; i++)
{
	var _p = _p_list[| i];
	if (_p.shown)
	{
		FillPrimeVBuffer(_p, _p.disabling);
	}
}

vertex_end(vbuffer_tracks);
vertex_end(vbuffer_tracks_mutual);
vertex_end(vbuffer_tracks_highlighted);
vertex_end(vbuffer_tracks_highlighted_mutual);