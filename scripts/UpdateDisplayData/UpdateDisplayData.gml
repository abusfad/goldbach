///@func UpdateDisplayData()

app_surf_scale = min(window_get_width() div view_dimensions[0], window_get_height() div view_dimensions[1]);
app_surf_dimensions = [	app_surf_scale * view_dimensions[0],
						app_surf_scale * view_dimensions[1]	];
app_surf_coords = [	(window_get_width() - app_surf_dimensions[0]) / 2,
					(window_get_height() - app_surf_dimensions[1]) / 2];
app_surf_coords[@ 2] = app_surf_coords[0] + app_surf_dimensions[0]; 
app_surf_coords[@ 3] = app_surf_coords[1] + app_surf_dimensions[1]; 

surface_resize(application_surface, app_surf_dimensions[0], app_surf_dimensions[1]);

display_set_gui_size(app_surf_dimensions[0], app_surf_dimensions[1]);
display_set_gui_maximize(1, 1, app_surf_coords[0], app_surf_coords[1]);