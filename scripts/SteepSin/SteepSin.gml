///@func SteepSin(0-2*pi)
///@arg 0-2*pi

return SmoothStep(1, -1, 1, sin(argument0));