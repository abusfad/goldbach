///@func ChangeHighlightList(highlight)
///@arg highlight

var _to_highlight = argument0,
	_highlighted = highlighted_list;

var _num = ds_list_size(_highlighted);
for (var i=0; i<_num; i++)
{
	var _p = _highlighted[| i];
	_p.highlighted = false;
}

list_Free(_highlighted);

var _num = ds_list_size(_to_highlight);
for (var i=0; i<_num; i++)
{
	var _p = _to_highlight[| i];
	_p.highlighted = true;
}

//need to duplicate the list so other procedures won't change it
highlighted_list = ds_list_duplicate(_to_highlight);