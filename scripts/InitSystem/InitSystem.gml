///@func InitSystem(n, layer)
///@arg n
///@arg layer

n = argument0/2;

primes = list_Create();
ds_list_add(primes, 2, 3); //for more optimised prime searching script manually manage the first two cases
highest_checked = 3;

primes_instances = list_Create();
primes_instances_cached = list_Create();
	var _p = instance_create_depth(0, 0, depth-1, obj_prime);
	_p.value = 2;
	ds_list_add(primes_instances_cached, _p);

strong_components = list_Create();
order_stack = ds_stack_create();

loops = list_Create();
loop_current_steps = list_Create();

InitSystemVisuals(DISTRIBUTION.even);
InitMenus();

UpdateSystem(argument0, argument1);

UpdateSystemVisuals();