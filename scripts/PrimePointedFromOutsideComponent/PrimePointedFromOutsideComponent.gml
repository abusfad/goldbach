///@func PrimePointedFromOutsideComponent(prime, component)
///@arg prime
///@arg component
gml_pragma("forceinline");

var _dis = argument0.disabled_by,
	_num = ds_list_size(_dis),
	_c = argument1;

for (var i=0; i<_num; i++)
{
	var _p_prev = _dis[| i];
	if (_p_prev.component != _c)
	{
		return true;
	}
}
return false;