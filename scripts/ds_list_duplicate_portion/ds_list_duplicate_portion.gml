///@func ds_list_duplicate_portion(source, start, end)
///@arg source
///@arg start
///@arg end

var _list = list_Create(),
	_org = argument0,
	_start = argument1,
	_end = argument2;

for (var i=_start; i<=_end; i++)
{
	ds_list_add(_list, _org[| i]);
}

return _list;