///@func InputNestedListsSorted(script, nesting_list)
///@arg script
///@arg nesting_list

var _temp = list_Create(),
	_nesting = argument1,
	_num = ds_list_size(_nesting);

for (var i=0; i<_num; i++)
{
	ds_list_append(_temp, _nesting[| i]);
}

ds_list_sort(_temp, true);

script_execute(argument0, _temp);

list_Free(_temp);