///@func AddPrimesInstances()

var _primes = primes,
	_pil = primes_instances_cached,
	_num = ds_list_size(_primes),
	_n = n,
	
	_depth = depth;

var i = ds_list_size(_pil)-1, //0 at start of program
	_val = _primes[| i];

if (_val>_n)
{
	//no need to create new instances, find the right index for primes_instance to copy until
	while (_val>_n)
	{
		i--;
		_val = _primes[| i];
	}
}
else
{
	//create instances to meet all needed primes
	while (true)
	{	
		i++;
		//don't try to get a prime value outside the list's bounds. AddPrimes ensures it has what's needed
		if (i>=_num)
		{
			break;
		}
		else
		{
			_val = _primes[| i];
			if (_val>_n)
			{
				break;
			}
			else
			{
				var _p = instance_create_depth(0, 0, _depth-1, obj_prime);
				_p.value = _val;
				if (_val<1000) _p.font = fnt_prime;
				ds_list_add(_pil, _p);
			}
		}
	}
	i--; //return i to correct index for the copy procedure ahead
}

ds_list_copy_portion(primes_instances, _pil, 0, i);

var _size = ds_list_size(_pil);
for (var j=i+1; j<_size; j++)
{
	var _inst = _pil[| j];
	_inst.active = false;
}