///@func ClearInstances()

var _list = destroy_on_reset_instances,
	_num = ds_list_size(_list);

for (var i=0; i<_num; i++)
{
	instance_destroy(_list[| i]);
}

ds_list_clear(_list);