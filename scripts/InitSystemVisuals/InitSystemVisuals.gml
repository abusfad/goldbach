///@func InitSystemVisuals(distribution)
///@arg distribution

vertex_format_begin();
vertex_format_add_position();
vertex_format_add_texcoord();
primes_tracks_vformat = vertex_format_end();

vbuffer_tracks = vertex_create_buffer();
vbuffer_tracks_mutual = vertex_create_buffer();
vbuffer_tracks_highlighted = vertex_create_buffer();
vbuffer_tracks_highlighted_mutual = vertex_create_buffer();

distribution = argument0;

empty_list = list_Create();
highlighted_list = list_Create();
show_list = list_Create();
ChangeShowList(primes_instances);
ChangeHighlightList(highlighted_list);

surf_highlighted = -1;
surf_white = -1;
surf_ping = -1;
surf_pong = -1;

u_time = shader_get_uniform(shd_tracks, "u_time");
u_alpha = shader_get_uniform(shd_tracks, "u_alpha");
u_time_mutual = shader_get_uniform(shd_tracks_mutual, "u_time");
u_alpha_mutual = shader_get_uniform(shd_tracks_mutual, "u_alpha");

u_glow_color = shader_get_uniform(shd_glow, "u_glow_color");
smp_glow = shader_get_sampler_index(shd_glow, "smp_glow");

glow_low = [0.4, 0.0, 0.8, 0.9];
//glow_high = [0.9, 0.4, 0.9, 0.7];
//glow_high = [0.8, 0.8, 1.0, 0.7];

glow = [];
array_copy(glow, 0, glow_low, 0, array_length_1d(glow_low));

InitBlurVars();