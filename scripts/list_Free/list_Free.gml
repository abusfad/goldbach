///@func list_Free(list)
///@arg list
gml_pragma("forceinline");

ds_list_clear(argument0);
ds_list_add(LOGIC.free_lists, argument0);