///@func BuildListHighlightedVBuffer(list)
///@arg list

//first return everything to the base vbuffer.
//highlighted tracks will be duplicate there but covered visually by their highlighted counterpart
UpdateHighlightList(empty_list);

var _vbuff_hl_base = vbuffer_tracks_highlighted,
	_vbuff_hl_mutual = vbuffer_tracks_highlighted_mutual,
	_format = primes_tracks_vformat,
	
	_list = argument0,
	_num = ds_list_size(_list);

vertex_begin(_vbuff_hl_base, _format);
vertex_begin(_vbuff_hl_mutual, _format);

var _pfirst = _list[| 0];
if _pfirst.shown _pfirst.highlighted = true;

for (var i=0; i<_num-1; i++)
{
	var _pstart = _list[| i],
		_ptarget = _list[| i+1];
	
	if _pstart.shown && _ptarget.shown
	{
		var _vbuff = ds_list_find_index(_ptarget.disabling, _pstart) != -1
		?	_vbuff_hl_mutual
		:	_vbuff_hl_base;
	
		FillTracksVBuffer(_vbuff, [_pstart.x, _pstart.y], [_ptarget.x, _ptarget.y]);
	}
	
	_ptarget.highlighted = true;
}

vertex_end(_vbuff_hl_base);
vertex_end(_vbuff_hl_mutual);

//for other functionality handling the highlighted list
list_Free(highlighted_list);
highlighted_list = ds_list_duplicate(_list);