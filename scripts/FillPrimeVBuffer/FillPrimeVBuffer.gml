///@func FillPrimeVBuffer(prime, list)
///@arg prime
///@arg list

var _p = argument0,
	_dis = argument1,
	_num = ds_list_size(_dis),
	_pos = [_p.x, _p.y],
	_p_hl = _p.highlighted;

for (var i=0; i<_num; i++)
{
	var _target = _dis[| i];
	if _target.shown
	{
		var _target_pos = [_target.x, _target.y];
		//figure if adding to normal vbuffer or the one to be highlighted
		var _vbuff;
		if (_p_hl && _target.highlighted)
		{
			_vbuff = (ds_list_find_index(_target.disabling, _p) != -1)
			?	vbuffer_tracks_highlighted_mutual
			:	vbuffer_tracks_highlighted;
		}
		else
		{
			_vbuff = (ds_list_find_index(_target.disabling, _p) != -1)
			?	vbuffer_tracks_mutual
			:	vbuffer_tracks;
		}
	
		FillTracksVBuffer(_vbuff, _pos, _target_pos);
	}
}