///@func FreeNestingList(list, depth)
///@arg list
///@arg depth

var _list = argument0,
	_num = ds_list_size(_list),
	
	_depth = argument1;

for (var i=0; i<_num; i++)
{
	if (_depth==2)
	{
		list_Free(_list[| i]);
	}
	else
	{
		FreeNestingList(_list[| i], _depth-1);
	}
}

list_Free(_list);