///@func GoldbachAboveHalf()

var _floor = ceil(n/2),
	_pil = primes_instances,
	_num = ds_list_size(_pil);

var _p_last = _pil[| _num-1];
if (_p_last.value == n)
{
	return true;
}
else
{
	var i = test_value;
	do
	{
		var _p = _pil[| i++];
	} until (_p.value >= _floor)
	i--;
	test_value = i;

	for (var i = test_value; i<_num; i++) //lol gml
	{
		var _p = _pil[| i];
	
		if (ds_list_empty(_p.disabled_by))
		{
			return true;
		}
	}

	return false;
}