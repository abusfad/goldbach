///@func menu_DrawOptionsSurface()

surface_set_target(surf_options);

draw_clear(c_white);

var _opt = options,
	_num = ds_list_size(_opt)/4,
	
	_heights_list = options_heights,
	_heights_map = heights_map,
	_opt_ui_h = options_ui_height,
	
	_w = width,
	_h = height,

	_col_bg = color_name,
	_col_fg = color_background,
	
	_focus = focus,
	_scroll = scroll;

if (keyboard_check_pressed(ord("S")))
{
	Nothing();
}

draw_set_valign(fa_top);
draw_set_halign(fa_left);

var _scroll_row = floor(_scroll/_h),
	i = _heights_map[? _scroll_row]; //shoud be defined in map because scroll is clamped to less than heights_sum

var _start_y = 0,
	_row = _scroll_row;
while (--_row>=0)
{
	if (_heights_map[?_row] != i) break;
	
	_start_y -= _h;
}
	

while (i<_num)
{
	if (_start_y >= _opt_ui_h+_h) break;
	
	var _rows_height = _heights_list[| i];
	
	draw_set_color(_focus==i ? _col_bg : _col_fg);
		draw_rectangle(0, _start_y, _w-1, _start_y+_h*_rows_height, false);
	draw_set_color(c_black);
		draw_rectangle(0, _start_y, _w-1, _start_y+_h*_rows_height, true);
	
	draw_set_color(_focus==i ? _col_fg : _col_bg);
		switch (_opt[| i*4])
		{
			case OPTION.menu:
				draw_triangle(_w*1/64, _start_y+_h/2, _w*5/64, _start_y+_h*2/8, _w*5/64, _start_y+_h*6/8, false);
				draw_text_ext(_w*7/64, _start_y+_h/4, _opt[| i*4+1], _h, _w*63/64);
				break;
			case OPTION.script:
				draw_text_ext(_w*1/64, _start_y+_h/4, _opt[| i*4+1], _h, _w*63/64);
				break;
		}
	
	_start_y += _heights_list[| i] * _h;
	
	i++;
}

if (ds_list_empty(_opt))
{
	draw_set_color(c_black);
		draw_text_ext(_w*1/64, 0, "(no items)", _h+_h/4, _w);
}

draw_set_color(c_white);
surface_reset_target();