///@func drop_menu_Close(close_root)
///@arg close_root

open = false;
scroll = 0;
focus = noone;

LOGIC.open_menu = argument0 ? noone: parent_menu;

if parent_menu != noone
{
	with parent_menu
	{
		inside_menu_open = noone;
		if argument0
		{
			drop_menu_Close(true);
		}
	}
}