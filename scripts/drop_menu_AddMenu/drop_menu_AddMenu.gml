///@func drop_menu_AddMenu(menu, menu_name, destroyed_on_reset)
///@arg menu
///@arg menu_name
///@arg destroyed_on_reset


with argument0
{
	
	var _new_menu = drop_menu_Init(-1, -1, "");

	_new_menu.parent_menu = argument0;

	ds_list_add(options, OPTION.menu, argument1, _new_menu, noone);
}

if argument2 ds_list_add(LOGIC.destroy_on_reset_instances, _new_menu);

return _new_menu;