{
    "id": "28a15973-a29a-42ea-83e4-b7bb6bb9a0fc",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_prime",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e4af46bf-9696-4cc1-98a5-fa51fc42167a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "21310cfb-7dc4-4444-b312-b59aea209b14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 210,
                "y": 42
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "ccfbeb1c-1b46-4c9e-8749-8aa440274ec1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 201,
                "y": 42
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "0b6116f6-29b3-47a2-9f5b-a9dac32d1fd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 190,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "75a12f05-1c41-412e-9d56-d1917c0ffad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 179,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "0d57dd27-da88-4320-8279-313098656f7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 163,
                "y": 42
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "435372c1-b581-46da-a37e-f0aff7b292d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 149,
                "y": 42
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "294b38a7-4016-415a-8a6e-ebc1e4873e3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 143,
                "y": 42
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c0fee475-0809-42bb-b044-5c7d86bddbd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 136,
                "y": 42
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "746b5fb9-4ddd-43d8-a445-cfa96f464c3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 129,
                "y": 42
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "cd0dbfe2-5d4d-498e-943d-52fcf08a37cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 215,
                "y": 42
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "75770c98-7624-477a-a506-6e06ce75c843",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 118,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ceacf9c6-7318-476b-9f90-723614a0a70d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 101,
                "y": 42
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "97fb5e12-b089-4e3e-9fa9-dd144d123897",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 93,
                "y": 42
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "1903ee6a-5959-488d-b9a0-bdec77ba41fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 88,
                "y": 42
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d0917613-639f-4430-88ba-ac589dab26ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 80,
                "y": 42
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9f82a182-f141-42cd-bdf0-6d3c7869cbe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 69,
                "y": 42
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6c20f6fa-90c0-43f8-92be-9b1e58ed2554",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 61,
                "y": 42
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "4fc5c315-2264-4e44-beca-748caf012850",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 50,
                "y": 42
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "988d5e2b-49e1-495d-ad4d-972dd889799a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 39,
                "y": 42
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f91720b1-0362-4874-b322-5fd2816e4233",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 28,
                "y": 42
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5ab4c8e6-cd64-45a1-a5d0-2aed9cf4a84d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 107,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "4171101d-52e3-4b7d-ae97-3f1db6de6d4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 223,
                "y": 42
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a746be1f-c458-44e1-80c6-958fbf97f999",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 234,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "98da5aec-2052-45e4-8a64-0f065b83eac2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "514b1d54-efc5-4978-897a-8577da7853b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 238,
                "y": 62
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "14d0605f-2e31-42c2-ac3d-89a68b8fb354",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 233,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "698a8430-ae53-40aa-a66e-1b70f49e3baf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 228,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "054cd978-6920-4b54-997d-4e2e982a7219",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 217,
                "y": 62
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c00080de-66a7-423e-9fe4-98cda279f912",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 206,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "b2084b4b-964b-4c6a-948e-ec358b844958",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 195,
                "y": 62
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "fee936ae-5006-41fa-a36f-b9983909a4a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 183,
                "y": 62
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "08968f58-bfca-416b-9a81-a444e6639313",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 165,
                "y": 62
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ada2c3f1-100c-4e88-9ce3-65f143409c1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 151,
                "y": 62
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "33349b58-cdab-4bc9-867b-f0efc36bc895",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 139,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "c215813a-4869-4b5a-88bb-65a2c8672ac8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 126,
                "y": 62
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "9f56fe46-65b7-4c7e-8f64-97781f6f8e21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 114,
                "y": 62
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "45cb41ed-bbc5-447c-8c69-ea05a739c780",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 103,
                "y": 62
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "faf49bb2-23c7-4443-8168-74cee0f4e3f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 92,
                "y": 62
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "7d776d22-2931-4d58-9082-f1fe0746078a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 78,
                "y": 62
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "ae7b78e3-dfc2-4060-b82e-aa24cb893a85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 66,
                "y": 62
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "486202c4-7fc7-4d7f-a999-0df7c053e5a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 61,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "00d2bc2b-ba52-425d-a04a-5d6e3acb2919",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 51,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ebb1b326-a8cb-45e2-9fb4-d36dbc73497e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 38,
                "y": 62
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "4e7b6fe7-399b-43d3-88ba-a225e037195c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 27,
                "y": 62
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f5dd5240-94be-4af0-b9c6-c6b0415fff5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 13,
                "y": 62
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "5f88b059-8449-4934-a338-efb31a201640",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 16,
                "y": 42
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0595c974-b1b5-4651-a9a6-5f557d899330",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "83918723-92df-4a83-95fe-9a3fabd4d131",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 241,
                "y": 22
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5e7d3a26-a1d8-4035-85cb-ed68ffdd47af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "590f2891-a57c-42ff-b50f-70f94ac23752",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "7c4bdbd6-fb81-46f2-a85e-5e12b01f472e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c1c3c345-1e91-4674-ac73-f19085f7c3ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e6dd372d-e5f6-46ae-b753-f42f70d94a53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "6f91040f-00eb-45ed-bd2e-f6efa60dcb0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "bd7d213d-7b8c-46b6-ab81-fc96e1cc02d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "8c7ed728-f360-49ac-b56a-1d24dfb3aa83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f220303c-879a-4d14-9e75-a848e6bfc050",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "a44a757e-af31-4d18-afc4-a9cfa3a13564",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b038080a-8ed9-4817-b4ef-1072e15f389a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d8654e12-4a09-49a1-aa60-0f6a096f472e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ade39713-a14c-40e7-9e47-f1681a824693",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "8744369d-0f4b-4c31-88c4-696d1ca3a730",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "dc8db4ab-c349-41f7-8b27-55d272ac2421",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9aeac6e4-335f-4769-ab91-089e1ad3569d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "21c49424-96e9-40d3-887d-237355b200b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "63299393-71ca-4245-a999-1c0a2142960a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "08d7e571-5f54-455e-b4dd-453072b6ecae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "202f916a-89e5-42d2-9be3-a183712c3bf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ce603b4d-d8c9-48ae-9e02-cdaff10bc8b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "770bb19d-d6db-4c48-89e3-87c8c52ee02c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ed3dc206-001e-46d7-b327-2f862d2f32ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 17,
                "y": 22
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d8cb15f5-e989-4cf1-9d82-2aa3543505bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 122,
                "y": 22
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e88b8796-04fe-4b27-8b7d-178b4423cb4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 28,
                "y": 22
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "977392a6-02bd-4339-aedb-93174dec28d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 223,
                "y": 22
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "eb0dff86-7f46-46f3-8182-89d984c4ac43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 213,
                "y": 22
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "5c4ab1aa-19c2-43dc-88b8-9ed6cd22c250",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 208,
                "y": 22
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "644d3f4c-8dc4-4ada-bc00-356bb0b2b53b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 192,
                "y": 22
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a8b32799-5a20-470b-9fcb-09b734d289e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 182,
                "y": 22
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "5af3ebc9-5161-4f1a-b460-b9f0f9a073a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 170,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "56ec0ee6-9f8b-449f-8150-e4c654645bb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 159,
                "y": 22
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "006888aa-ecac-477d-b1b6-e597057c4a45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 148,
                "y": 22
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "06cc762c-9afb-419b-af49-109cb10d7a03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 140,
                "y": 22
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "6c60483f-1e94-4bba-91f2-77c5ef77b1f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 230,
                "y": 22
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3457ee6e-4c89-415a-9a58-12ab16dcaddd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 132,
                "y": 22
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "edf0ca78-1666-4184-a431-b6c8264ab5c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 112,
                "y": 22
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "31431446-b9a1-4cae-8f6f-4028d06768e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 22
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "b0254b7a-2940-4d4c-9f23-954fbe8fb6f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 86,
                "y": 22
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "932dfe66-0502-4f4a-ba8b-0abf91f47843",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 75,
                "y": 22
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "70aa3108-5b0e-4d23-9454-f1e758090ab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 64,
                "y": 22
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "17bd7afe-2f46-4126-9f51-e86a52ba81ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 54,
                "y": 22
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "8da49323-c0bf-4097-b877-6e575504aaae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 46,
                "y": 22
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "81643c82-2a1f-481d-9c23-eaf587f844d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 41,
                "y": 22
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "821876bb-3f25-40b8-a324-06816b600e2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 33,
                "y": 22
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "275b79e0-4e51-4c35-807d-ca02bddcd6e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "abb36692-67c9-42a3-b58c-23f539202788",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 18,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 13,
                "y": 82
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "706b0d50-ccf6-4238-856e-ec14e879811a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "3a053dc4-c0be-44a6-b7d9-daf31cb27ece",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "b2160e6c-1716-471e-a170-89db51b637af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "c03fc0cc-e402-48b2-8133-51f7f65ae02d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "f9a347b2-54ab-4303-b048-deaab66b381c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "12c4450d-6086-4884-99e6-87398f1a666e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "9a5a5dd0-d8c6-4b62-819d-26334cbcd136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "ccd59d0f-0d91-4857-8dcd-a2c08115df6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "1383f3f7-55df-4390-8f9f-7268ffbcb847",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "44df9b9f-f115-41d8-a536-cf6eeddebaff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "f967f899-2c96-40cc-8bcf-0194971dd6c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "dc07bf20-882c-4cc6-bbe6-beb0d07e12f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "c450e910-fada-486b-8b81-bc227ecb62c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "acbe26d3-2b28-4b4e-b24d-ee7143f737d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "4f487955-f4c3-49d5-90ab-8a095dc59e85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "d082412e-bffd-4474-a3f3-876f76c41a1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "ee6ec33d-4071-47b6-82d1-406947d2de0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "90338c43-2f09-44bc-9380-f9b8260fbadb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "d745009f-5dc2-427e-b1a3-a0681a69057f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "35c225dd-6e22-488e-9dbd-74c7b20b7d6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "183738f4-45f9-4ada-bf79-3eee5e82f79a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "62c64301-f50e-485d-9cd9-dcb38f238060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "883822cb-60aa-4989-baf7-f81b30935344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "8cd60926-bfd1-44c0-898a-54eeeac676c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "e3586771-c8d3-4c16-96e2-078619638d1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "3bf68cca-dac2-41d9-9328-471a08b1b7e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "bd5ab3ea-9b8f-4e9f-924d-40e8e5aaa97f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "755f2478-90ee-4cda-bbfa-bd7944577c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "fef09706-614b-4c2d-b0c5-aec0fc0e488e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "27f34cb2-6980-438c-b817-064841579d61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "bb45c92a-832d-4d01-93ea-63e17c1e3932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "f445fdca-9945-4081-afa8-39e96712fbd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "444680f3-5021-4c6f-95bb-e574a391ef8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "adbf6a46-d7a4-428e-b4ad-a1b3fb704be6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "00c31fd3-c564-48a0-9ebd-4f285eb001ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "50ca41f8-8822-42b0-8ad7-b9d7a80a4cc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "6b76a997-be55-45bb-ab83-7194560eaf90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "56367ea9-a53e-40c9-87a7-18e3bb711744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "a326f7a0-9b01-4b57-806d-4f55528046d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "0480d446-5377-4459-bc3c-4119c9fc71e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "82e485c2-576d-4289-89b1-2e2a09e15c24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "acaf038c-4d7a-4221-8e20-8d667988be46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "d2ec6eb8-401e-4de6-a7cc-b0a025e76536",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "19925cc6-980b-4051-a594-6a0e43771ee3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "5e3d69c4-2e23-4f25-a414-78b723885df3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "03e27224-cb92-4d16-b0b9-c264319d168f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "5b5e7445-8e50-4dde-ac45-166fdc92e448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "6d02d61d-ecd4-4935-99c9-ad48d11a2069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "7bdaef1f-825c-4cd3-b8a7-64ba3e655ef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "a3ad4c16-d00d-4611-913e-f0aa2a27aafc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "f7e5bbb0-991b-4d1a-9162-59a1733e94c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "13567745-4d06-46c9-b6db-f20ee92cd040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "628e8473-5212-4655-b934-2ed03d6391f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "449a7a30-a0d2-4aeb-b04a-7bc00ff04316",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "26051507-d0ea-4e79-aa24-94bea284f41d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "9b5d7efa-40c0-47d7-8299-7a6831cdd1e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "32046242-e9e4-42de-84a9-3929cb081a68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "e55f902c-8e39-45b4-8dd8-59449e9cda8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "bafef261-884d-4631-92c1-f805afdfd2b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "b78da408-cca7-4337-a76e-7512405ddd13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "e9b91139-3b9c-43c6-a126-88a36567d717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "2d9fb76f-f65e-4afa-8caf-3e4a0f0d171c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "f6e7cd60-cb9f-4495-a187-296a1116ab91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "9f7dbb76-18e4-4498-a4cd-e3b16adaa2d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "56a7e400-16ae-49cc-b40a-8ed50cbc6882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "49a62811-9eba-448d-b917-fd6461443e27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "52e509d4-370c-480f-a31d-1ce417433e55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "10e61edc-e52e-4a5a-979e-f9ef0d423b81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "2975bf03-bfb2-4e23-87a0-5a1fe61c1b0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "157761b0-15d2-424c-bff7-dab8740c61cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}