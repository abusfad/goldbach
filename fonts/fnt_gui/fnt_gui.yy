{
    "id": "0cc36af2-8b3c-4d96-8d89-12d8083ba312",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_gui",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4b92d99e-62c1-4cc9-9c26-73e9975105c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "26cb053a-a889-4c2c-acc3-b0418d8e2c16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 165,
                "y": 42
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "bdd3a4a6-ae23-4e9e-b271-8f57499a5c2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 158,
                "y": 42
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d24ae39e-5eee-4f20-a2c3-370b5e6db193",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 147,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "2e0df80c-24ea-4294-8b51-743c6ef90fb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 136,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e4cb3509-72a3-435c-bc12-e1cfe3c70938",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 120,
                "y": 42
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "84bc6820-4195-4b69-a4a7-1312b5f0c105",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 107,
                "y": 42
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8d669cf9-34ab-4f47-b94f-fba501d16825",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 102,
                "y": 42
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4874d9b9-43b9-403d-a547-02c419ad4070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 95,
                "y": 42
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "1ebbb289-1b62-49b7-845a-46b0dbb7af21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 88,
                "y": 42
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b0bf3bc0-0b35-4142-8fd4-ebc59fbd2523",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 170,
                "y": 42
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a3fe26fc-a737-43fb-b2be-8a3c4102466d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 77,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "5ae163cf-d401-45af-9491-29579840037d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 61,
                "y": 42
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "05c309aa-d5fc-4714-afbc-92a0ab1076f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 54,
                "y": 42
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "82579cfa-b640-462e-bba8-c07b0bca09de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 49,
                "y": 42
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "33c367a1-b148-4500-ac75-35e4d9675217",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 42,
                "y": 42
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9131a4e7-5f7e-48c1-8a37-132fd0467d3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 31,
                "y": 42
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0d7ea0b7-8f1b-482c-b628-041c599fbf08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 24,
                "y": 42
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "977d3149-3caa-423a-800e-e33535486338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 42
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8c5da0dc-d455-4822-b493-1685e17b4187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "6bd075ae-e7a0-460b-bac1-f9ce4b0a8da0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 240,
                "y": 22
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "1285babc-e40c-4989-8b7c-937e08f6bb7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 66,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "fb2cac36-e7dd-42ea-976f-f8237a690edc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 178,
                "y": 42
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ef12d854-f77d-4914-ba34-767196cde5d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 189,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2f29023e-59ed-4ab1-915b-918b92daaca7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 200,
                "y": 42
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b389d104-3af3-402e-a3c6-7fb1057e33e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 186,
                "y": 62
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "bd4de52d-9d60-406c-a51c-8eb5ab628244",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 181,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f2cab91d-0836-4972-bedf-3757294653ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 176,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "52620591-b25e-4d7e-a25e-82efe663706e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 165,
                "y": 62
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "e99a2c6e-99ef-4a77-88b6-213c3c999c34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 154,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "07123678-9a26-4e37-b17e-7d3b3fcbae88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 143,
                "y": 62
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "71a70e45-e7ea-4d5c-89b3-a1fa54da4dbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 132,
                "y": 62
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c5dd1af8-9d24-43a5-81d2-e66b02359adc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 114,
                "y": 62
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "3a5efd16-1cd0-4ebf-af8e-53155d40aaf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 100,
                "y": 62
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b82ae7a0-26fc-41a7-a6be-a2aabaff0a8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 89,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "d9122d7a-c11a-4bd6-84cf-cc13a0cb986a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 76,
                "y": 62
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "6d4018b5-7791-4ec2-b178-ac7af993f403",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 64,
                "y": 62
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6245e693-15af-42ad-95f7-d69001215781",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 53,
                "y": 62
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "8c5969ca-d5a8-47bf-ab3c-0ab619ede240",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 42,
                "y": 62
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "23ae194b-6986-42af-9184-4233f795405d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 28,
                "y": 62
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8d5404fa-5cfe-46f0-a0ed-e3b7351d1f24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 16,
                "y": 62
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e1bac235-846a-4da0-a694-f237378e967a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 11,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a797379b-53e9-47c0-82b0-1fd0709eea29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "8d36b38d-28a8-4aeb-a24b-85e658e847da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 235,
                "y": 42
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "39892fce-5741-433d-a214-a957da816b22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 225,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "cd352254-2cc7-4aa1-b46c-9e8ec433fc1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 211,
                "y": 42
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "5398f8a8-7dbc-4e05-bbcb-b55e59090756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 228,
                "y": 22
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "ed1ec975-4adb-4a69-a769-4ccbb476c0f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 214,
                "y": 22
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "936e33db-ed37-45a0-b35d-7b794135f9be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 203,
                "y": 22
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e4b27259-7aae-4db7-a6e1-6e9cc639e1d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e675ff97-8341-447b-af49-1cc92e5fa241",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "668c611d-cc35-46c4-9b80-720de63bd767",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e3e945da-3b2b-4072-a376-f08c244502af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "bc16c9a3-083e-4eba-83f7-b8a83ab0c0d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0713ac91-a426-4df9-8f40-62deb0d2dc74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d666df92-8579-4bf7-8641-956612448362",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "64020a38-273f-46a9-b6b8-c335893e70f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "a029c67f-733a-4353-abfa-186220ba347f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "0e3fb0ef-279b-49c5-9966-5f9d52c673fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "43410fd8-a1d2-4d8f-9ef8-c04d763848cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e11e87b1-782b-4cca-8ea3-fd455bc8ed9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "42bd1775-5c7e-46d5-b256-7448c7c0e692",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0804c71f-d3cf-4489-ab5f-e65239fd5a2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "fccac958-7723-44dd-8569-57bfe32892ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d434e79f-e495-4288-88bd-844fca824678",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "3ec7138f-3f50-4595-bc02-c0083f0aabc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "87bd7e0a-0b30-4427-8c41-08fc85d6835d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "41b47c02-5ca9-4e03-a5ad-b3c8a68e36e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b3e5939e-1cd9-4e3b-95a7-51161678c73c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "328ead4a-78ea-46e1-bca0-f3667b6b4040",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "8adc823f-4034-479f-834f-35561b684f79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "cec4239a-0777-4845-8cc0-24f7f54206c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "2d8c5221-9e80-465b-b652-3de1017e91ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 97,
                "y": 22
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4b0c51ec-a49b-420e-9cbb-8eb7dcf3170e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 12,
                "y": 22
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "96c7ddd8-be58-4ed0-a9bb-f720d25157f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 187,
                "y": 22
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "53aa3b41-266a-404f-9b7c-cfa1a94e4477",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 178,
                "y": 22
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "07dd54e0-5e82-4936-aa7a-cadf800faa58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 174,
                "y": 22
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "b8d9d730-ac8c-4474-a320-e0b0cc54ed29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 160,
                "y": 22
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a19d4968-5eab-4f4a-9811-47bb88d7141b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 151,
                "y": 22
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "dcc9a750-cda5-4781-876e-17d49cecec62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 140,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "91f93cbc-b571-433a-8ac9-48da6124f26b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 130,
                "y": 22
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "243460fd-795c-470f-badb-6bebd38675d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 120,
                "y": 22
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "43741d6f-8bb6-40dc-b52c-0e5c28284db9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 113,
                "y": 22
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "5cd4ab7d-76f5-462c-bad7-453250ade287",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 193,
                "y": 22
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "9a3b994a-6ad5-4151-8ad3-d948e66cb77c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 106,
                "y": 22
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9381194c-7213-4fba-ad92-496bf5637107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 88,
                "y": 22
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "97e49921-c120-4254-8c0d-df183f6bdcb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 78,
                "y": 22
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "b7bc421e-1b9a-4c04-8141-fc663f088d4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 64,
                "y": 22
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "36c0bc75-3e35-4c88-83c7-d409886bed9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 54,
                "y": 22
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4d5cdc8b-5b61-49d0-8be7-d7ef5770b96b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 44,
                "y": 22
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "58556189-f3a4-40b0-a8f7-72c2f01b92b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 34,
                "y": 22
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "15a5b1e0-3f4a-445e-9406-a46f7f4f74c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 27,
                "y": 22
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7b83fbea-4e80-4712-a4e8-30575bf67788",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 23,
                "y": 22
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "dc648e70-045b-46b4-997f-4f5f2a66ff12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 22
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a0abfd51-3ca6-47a5-af1e-655eb499eedf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 197,
                "y": 62
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4baf3b91-953a-4693-96b4-4c2fc1810375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 18,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 208,
                "y": 62
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "8a783d57-da94-49da-8229-70403a648107",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "e1226166-de58-4209-be40-10568bbabf7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "f1337d4b-5d7f-47a0-aee4-f38e6b99e8be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "d522429c-8612-40fd-9f8b-3f072125ea59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "857e1403-01e8-48ac-ab48-38d926a2ad3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "05ac4bcc-f799-4c13-9b21-2326ad4b6972",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "fb39b12f-7cb6-4e44-9208-aca9a257b9a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "7aa9174f-ee85-4084-8c53-54d4fb5e782f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "43818b51-c3e8-4bc1-8184-6a160b545d26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "8952057a-9544-4efb-8779-221f045518ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "8ab01859-271c-4055-adb1-dd710c8f9e36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "6de394b7-e821-4ea5-ac38-0f79c9b09ea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "db1150c7-b08e-4286-bdf6-0dc6095add41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "f5c8846b-efdb-4209-93ec-0cc39e3ceadf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "6d44ce11-0bf8-4de9-be59-bf205a5a0f8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "1b8b3b53-1125-4acd-960c-e85d33e1e05f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "b72ab9ea-14f6-4fb9-a047-5bc0f3384765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "f7e65696-30e9-4182-a1e0-e48784438166",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "5935c5c3-5e03-4e37-a9a3-09fe45ee8457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "f5098170-1fb6-4302-9260-87314c1ff318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "2d52bbb9-0577-4053-a9f4-fa24e58a3204",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "2078fabf-5b6f-4743-8382-0a405f094ba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "421226ad-039f-4aea-beca-e47123b464e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "cf49788c-c012-4a9b-90d5-41aaefffa579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "4339a779-966d-44ef-bcce-2ce03b9e8512",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "46fe20d6-c74e-415b-8dad-c3603b91582b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "77794771-3308-4343-a4c0-e7e4200edc18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "93f47798-714a-4e63-b73f-0821979de378",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "d54af9f4-a91e-4d09-a9a9-92a27c20071c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "c4111e15-5a7f-45d5-8f3f-cc5825c8b5c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "5567fbdf-94f9-450f-9e83-d26b0c8d7a1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "d0373fe2-cb8c-4e75-90bf-327f26f9926e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "18daeb7a-d22e-4811-b758-f613b4a28c27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "92a5fdc2-e96e-4c29-ba64-243919e8f182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "af98e476-628e-4592-b0f9-c3d431e290d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "d196bfe9-b87b-453b-8509-2030dd3fd478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "7a46e56c-adcd-4ae9-a6fe-e4059de2c046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "c568e3b2-77fe-4dad-aec0-c9acdb312197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "a4f7326e-b8f8-4931-8c28-8dfa9beaf7ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "7a88be08-2826-4691-9b56-3c03e8cf3ac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "2611c997-9aa7-48c2-8ee3-2c85cabfd71f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "c3b769c7-1984-4cc6-b6ac-eff986a9424c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "fdc3759e-ebb5-41a0-b57e-a8fd6ae03b3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "e860a06a-c7f0-4993-91d7-e244febf3be4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "d9db2342-e198-46e7-93a7-b6939a7aa876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "3e43fe0d-216e-4662-b606-bfd065409d9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "7546f01e-3182-4b27-ae4f-6bd66df6316c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "b125905b-1c2d-48c2-85d1-c67debd2cc75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "4ffd9ef5-8f90-4e92-a1a6-04c633c65d8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "2126c7ab-8fe2-496d-819b-003543c58956",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "ee70de62-c964-44f3-a523-6125d1390df5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "f3c88b36-dd01-4a33-97af-de9232be7d3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "8185c779-7c95-4970-afaa-a86f7cbaef53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "f894265a-ff5f-4179-8585-62130ae341ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "e0015e5d-54de-4e75-89c8-265fd31727af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "6850b369-d804-45d4-8b5e-f28193ebcab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "277473ac-52e1-4ad5-ad71-bca78043dc39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "b128ba4a-0005-45fa-9809-ace24c855f8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "9a38b673-e1c6-41aa-9c32-fb4d31e32df5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "5ddd5f33-60eb-434f-8682-9b790ec1686b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "7e6a1bcf-0a0f-4474-8369-17946087fd7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "9ed7ceb6-3b2a-4d0e-b843-ba22b6787d35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "7bebe7b8-81e8-434a-baa1-6a64d8a9b090",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "234ec51e-c964-4cfb-85a2-c79981d55242",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "609ee78c-5ee7-4c52-87bc-b735f98744c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "41cd4f1e-5578-4d87-81ac-57777cd13d13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "20cfc9a5-98b6-4e64-a985-c8fb8624b666",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "a426cef7-4bbb-4aad-8a37-41b75792b25a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "71bd0805-f6e8-4168-82ae-1d81b7d227cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "07207172-0876-49be-ac40-fc35e7ff6f48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "ec987dd0-5473-4227-bedc-17288ad5fb27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\n0123456789 .,<>\"'&!?\\nthe quick brown fox jumps over the lazy dog\\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\nDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}