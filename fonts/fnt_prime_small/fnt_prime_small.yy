{
    "id": "141490dd-b661-4bff-93fe-9a6845750a45",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_prime_small",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "734577f2-595b-42a1-a9f5-25ed810a1598",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3e497b29-fb09-4954-8275-021907e746ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 85,
                "y": 70
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d2714239-6ab9-4152-8f8c-a89f3ddba379",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 79,
                "y": 70
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "fc229023-7ba0-4922-9aff-12d0b30ca639",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 69,
                "y": 70
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "c71a1dd5-b50c-4094-9d73-880fea9b3047",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 60,
                "y": 70
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "393c5296-5628-4149-8b51-83e126ae3d79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 47,
                "y": 70
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "500031f9-2a3f-4dab-88f3-47b1a1ea1b56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 36,
                "y": 70
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c39a51f1-17fc-4a05-ae93-63e4a435337f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 15,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 32,
                "y": 70
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "32a2e0ac-6b05-408a-b379-910c44035d8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 26,
                "y": 70
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "bdb1b522-d7e5-41f6-82fe-ae4288a36688",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 20,
                "y": 70
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b9b6bbc2-a87e-4c69-8f49-54cb946e3e77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 89,
                "y": 70
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "401af627-a765-4918-b33d-80eaa541acfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 70
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d8e2eb2d-1b92-4dd2-a036-e30af2b71668",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 115,
                "y": 53
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "3a5d26a2-9d49-4117-a959-692edd46e88c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 109,
                "y": 53
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "487288b2-129d-4b59-bfe3-7fce517cf96f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 105,
                "y": 53
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "4d5b4a92-f772-48ac-861a-de2a111817b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 99,
                "y": 53
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "bd37afb8-52c0-4319-8c7a-c0ecb7364274",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 90,
                "y": 53
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a1850d28-b8d5-49ee-985e-c9e9d50d88e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 84,
                "y": 53
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "63be9f45-d519-44e7-9ebb-335720b94d15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 75,
                "y": 53
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "52707fac-4f24-4fd5-adf2-ae96634757f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 66,
                "y": 53
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "2b1d09e9-b08b-4a65-80b0-709f44fff430",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 57,
                "y": 53
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "18576b91-3f04-43a2-9c02-fca8e5ff0203",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "4e3651ff-2f42-4f07-b862-be0b095e9839",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 96,
                "y": 70
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8a06a930-7431-4540-ac73-5234f650b23d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 105,
                "y": 70
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "c0f19938-b8df-420d-a7c4-8181e0c4012c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 114,
                "y": 70
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "4925af06-5147-4d91-9045-16b5a0e9eb8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 73,
                "y": 104
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "36d01517-6a51-40f3-a736-73614d399d75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 69,
                "y": 104
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "8cc7a350-ac37-4528-ada4-98b3e305768d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 65,
                "y": 104
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "2f1082d1-14ed-4f59-8cdf-051c5b2a3042",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 56,
                "y": 104
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7b65a58c-dc9b-462d-8343-b183f28b0fb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 47,
                "y": 104
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "9a5f57e5-1426-4dea-bf43-3a8f6c227da1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 38,
                "y": 104
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8dcf1638-6434-470b-917e-472784701b9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 104
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "dff5a616-487a-4023-a5ff-574c53dacbac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 15,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 14,
                "y": 104
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "fa5070e7-a42c-4431-b0fb-401d07403ad1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 15,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e2a5ad8c-b713-43bf-81f0-62fe77d73177",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 107,
                "y": 87
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "8dbcfed6-fdb5-4301-ab55-a935f3944bb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 96,
                "y": 87
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "6f135a10-7794-45ab-968d-17c015305614",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 86,
                "y": 87
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "da7c47d8-de9f-4fe4-a10d-8af58826456c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 77,
                "y": 87
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "8c30b111-8f0e-4c41-a4e1-b31c2973bc56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 68,
                "y": 87
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "a1345b52-87cc-4adf-a717-40cea6524bf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 56,
                "y": 87
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8403d4a4-cec7-4184-a9f9-e14f500b8100",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 46,
                "y": 87
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "7e574b0f-08d1-4d27-b031-96028bdef184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 42,
                "y": 87
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "53ad0fcd-6d56-46a4-bea1-ecc0f6b50797",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 34,
                "y": 87
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9f4262a7-c43f-4457-a195-c59a16b26519",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 23,
                "y": 87
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b869768d-a65a-4bca-9c06-3d9338313a70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 14,
                "y": 87
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "52e025cb-a227-4255-a93d-8e2d42da2065",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 87
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c5b97ba3-8dcf-417c-b59d-26f192f7f8f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 53
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "4689f539-7977-4cd2-bc20-e026527ddad8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 34,
                "y": 53
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e625a353-c04d-4b78-b745-5d3343b2766d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 24,
                "y": 53
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "0be16388-774b-45f4-957e-7b9bf4778012",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 75,
                "y": 19
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "73efe848-8453-442a-8571-315a8bc445c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 58,
                "y": 19
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d1566ebf-a559-490f-b8f3-34aa2144a768",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 48,
                "y": 19
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "7497d188-63e1-4de3-af09-dd04347314c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 38,
                "y": 19
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "4d5aac2c-835b-4334-91c8-df4d6a971ce3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 28,
                "y": 19
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0625e825-3d3c-4496-8915-f72fe4d38764",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 17,
                "y": 19
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "0e220231-e622-4e79-8309-9cdb948bc730",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 2,
                "y": 19
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e779e957-f63e-4d1f-bd9f-b98840e35afd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "193c2e70-ec93-4e16-87ea-66188de3fd48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9be3f82f-b317-479d-9c8e-b9a49dc8e045",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7c3abb71-a0b6-4559-a19a-0ca1e53774bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 69,
                "y": 19
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "6edec854-391e-4753-9363-6a198332c6be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "aa4ccef7-3a22-408d-b350-66fc6698c7ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "4f6169d2-2966-42a2-a0b0-f910f78fb503",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "33e9b449-f53a-41f7-a9ea-dc03c348fd36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 15,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "eeb84231-df10-4e20-88e5-bb7ed4a01f47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "66241e06-b054-4936-8766-84694681e0bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "e2d4d1b2-215a-4955-a579-246f9146ef02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e878f9d5-80dc-4476-a236-0fdd15e9430d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f5384a63-129d-44f0-a418-d74708cbd15f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "238ab2c3-ae2e-47a4-8543-54f9a0b21163",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ed8d1478-fcb6-410c-842c-6de6ed19fbd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9e0357f0-74c6-449e-87fa-d4ea8e046c36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 87,
                "y": 19
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d9d722bf-d4e2-40af-949a-0554f774808c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 50,
                "y": 36
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c2360bbc-2a0f-4660-a560-f74c6321914b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 96,
                "y": 19
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1765aebc-41bf-40fb-879b-2901be60f765",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 15,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 11,
                "y": 53
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "273c0e40-64c8-4908-acdf-c4830b88add4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "27838d9b-8755-4827-89cb-1a1c56dfe761",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 120,
                "y": 36
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "025c6528-ea26-4fb1-a418-0e5834691aec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 108,
                "y": 36
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "cc04aa69-052b-406f-b1d1-37ea40e1f630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 99,
                "y": 36
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "8d3bd53e-e66a-4a7f-8ddd-ab93d478ba54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 90,
                "y": 36
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "95148a1f-bcc4-4ad9-b309-9231ce02d383",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 81,
                "y": 36
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "60a769cb-e8d1-430d-b411-73f0c12036b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 72,
                "y": 36
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "fed3340c-8ec3-47fe-98b5-d11ffd5ef69d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 65,
                "y": 36
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "f352c17b-dd6f-41a8-801d-81fa64193c58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 16,
                "y": 53
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8f086b2f-dc90-49fd-9e3a-1d2e57e1534c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 59,
                "y": 36
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a6df62d4-093c-4360-a268-6622c66e3c09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 41,
                "y": 36
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "78f599d4-1d99-4142-869b-1bf7e1dd0069",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 32,
                "y": 36
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "444b9159-b347-4141-bec5-4151323d0377",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 20,
                "y": 36
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "49f24dfc-0da3-4376-99cd-0aa7a4015aff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 36
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "16955adb-62f3-46dd-948d-6353ab4cff06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "8838a2c8-ff6c-4472-8884-bf6aa8cf4f01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 117,
                "y": 19
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "336aee53-daf5-446e-9d66-6b257bbf1ba3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 110,
                "y": 19
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7c33a4c7-0b9e-423d-9ce4-062e7515f0da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 15,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 106,
                "y": 19
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "dab33ae4-99bf-40b7-8a27-472c2d10dd8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 100,
                "y": 19
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "cff2e6f8-f9e1-4c78-9d55-71de30c35130",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 82,
                "y": 104
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "07057bf0-2028-4a1d-8934-7c0e45bb8421",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 15,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 92,
                "y": 104
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "4a092db5-2bc1-403a-b411-18f89b04d3a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "42dad53a-781b-4e82-a1c3-8823ac608b6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "0ce2309e-3f52-4de6-95ac-fa0f4e98b976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "805701b3-6ed1-451b-9028-97b5d4dafe99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "c6770aa5-3ba8-46e8-b276-bf25d6335695",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "8d7cc35e-2229-4237-b824-4846019d68e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "633557e9-aaa2-43d6-a49a-f065517a1668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "96d4f994-e2bb-43fa-912f-b184dccdb803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "2275207c-49c8-4ebc-bad0-8f00fea1ffff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "a0bb7e64-7d65-48c1-a7ab-3f10369938ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "dc837ed0-3311-441a-8ee2-c8a5bcfe138d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "e5803483-87f7-479d-97d2-1defa709a41d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "cfcaa7fe-bac1-470a-a6fc-d7cfacc4e75d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "8c238c22-6841-452d-8f96-28b7cf95e30b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "23079ddf-c5c0-44d8-b3aa-a959d13e4f6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "b869bf37-0c24-4469-b6ca-d04d5a98def8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "5ddfdab4-f5a7-4f68-a6bf-bda7dac9a3ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "ff626337-5b7f-4de0-ab27-ea5aa42de38c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "7ebefd30-d5e6-4516-8ebd-fc0a79006c78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "a6fef550-dd08-4a56-8528-b35ca83fd37f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "5107d516-d036-4990-9e49-c46752b0361f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "577e0a82-eb6f-4310-9ecf-cee2418d9a88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "2ca36077-4f21-4cd2-96cd-99557eae07aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "f3e203dd-c23d-4517-917d-306c7aee07c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "82a4686f-c5fa-4aa7-bc4d-af785236d27e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "f0a1947a-efc7-4c5f-b995-191d5124b583",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "38083425-8d0a-4a3e-8ebf-a9567ddf9155",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "bc59b259-8a36-4f3b-a6c0-091b11217c64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "dd3ac0aa-8a21-4321-ad46-ee289526df55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "7430f135-58f0-4d7d-aff0-434e6cf7bae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "a8c05f76-4a91-4a35-a9b3-8d49c238cc02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "f502011c-bda8-49ec-b4e4-5aff50a367dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "876c0080-785b-4e7e-896a-fc5b04ff2414",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "6496ece4-5da4-4c7e-a392-f12ce18b9dca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "228f3b15-dc20-46e4-bff3-193fdb627f9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "3370e1fb-38da-44cc-b2d2-a0649bc40078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "ea739385-2b5d-4dbc-8159-018c84ba0319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "16a79de6-f22c-49f9-968f-b4ed8a7fd62e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "7295968a-53f2-4eb5-af82-6a6774c35d94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "c953d5fd-7945-4c43-9b68-40a8266a9cdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "3163c5aa-f414-40fe-8d63-78a3d96403c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "6800873d-e967-4813-b583-5ecd209b1609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "a4bb0058-7bc5-4e18-b915-64a5df646b9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 10,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}