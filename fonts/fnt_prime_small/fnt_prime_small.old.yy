{
    "id": "141490dd-b661-4bff-93fe-9a6845750a45",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_prime_small",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "d25c3a26-1a35-4bd5-a503-b6ebe6f336b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "74e80c70-dd3b-4db2-b8a1-7899bf1ecb86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 123,
                "y": 36
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d45efc49-f91b-447d-be11-9daa64950445",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 115,
                "y": 36
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "82f6d179-3ffd-47ff-9bd0-3985c9d70d73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 105,
                "y": 36
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "2deb49af-93a6-41f0-bcc8-09d9875d0be9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 96,
                "y": 36
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "b86c64dc-d79b-414f-a518-98ce7adb60cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 83,
                "y": 36
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b43cb6ed-a863-4596-ba0c-c4c7d6371f15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 71,
                "y": 36
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c881eb71-4ccd-4b17-a67f-46335c506a83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 66,
                "y": 36
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "fc666599-c0da-42fb-8d86-5bb9ef641c94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 60,
                "y": 36
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "1ffef272-fae9-447a-bf29-916fb2a90d58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 54,
                "y": 36
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "937ed58b-530f-4850-8e5f-3b0f2f139f1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 128,
                "y": 36
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "fe664cd8-481e-4082-9da9-d8fb2456ae80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 44,
                "y": 36
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "36855c5f-d864-4500-83aa-0a9ad0cd375c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 30,
                "y": 36
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "24ebec8b-f059-4ad9-a270-25d194d321bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 23,
                "y": 36
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a8dd6f0a-e7c3-4de1-b51f-5fef8c23ab7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 18,
                "y": 36
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "809477e3-1fe5-4088-b1bb-dcf633586dac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 11,
                "y": 36
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "c2c092be-8b3d-4707-bb86-a0b1cee0b2c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "18bb9274-fcc8-4f9f-851c-494385fd0541",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 240,
                "y": 19
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1fe09f76-dff8-4ab0-a8f5-9283f8482976",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 231,
                "y": 19
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "2d698545-21ae-453f-a520-547ce1f5abad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 222,
                "y": 19
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "1291d42f-2168-4089-8dd1-6dbc1fa9ae12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 213,
                "y": 19
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "6f94c098-e819-41e4-9e2e-6f71aa9f0520",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 35,
                "y": 36
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "53fdb843-d36c-4703-a4fd-1c11898665f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 135,
                "y": 36
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "2466aae6-b6ef-48b6-a9e9-45eb7dc452f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 144,
                "y": 36
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "c015e9b3-ce26-4858-80ef-088b4ed0aa24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 153,
                "y": 36
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "08cd83c4-8d1c-4334-a2a1-bb0920e05e9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 110,
                "y": 53
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "948a371b-9782-411c-80d7-e5ac9f54c94c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 105,
                "y": 53
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d8104527-84a4-4952-836e-91a7698e72dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 100,
                "y": 53
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "74030658-6377-4ef4-b69d-7018bf03fdcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 91,
                "y": 53
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5a6ae6c1-9f1a-4bbc-a126-410f87c79112",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 81,
                "y": 53
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6a04e237-6cea-4d12-81b5-378aa007ccc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 72,
                "y": 53
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "5183c63b-837f-4b6d-9136-d6c6d6e9db91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 62,
                "y": 53
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "9b92618e-2e77-44ba-a2bc-48fd3682dec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 15,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 47,
                "y": 53
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "db9d1db3-ae37-48cc-9a69-2dad3dbcabf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 35,
                "y": 53
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "886fde46-99bd-4fad-92fa-b91213b28527",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 53
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "0d9734f8-a13d-4b60-8f36-f0c1db04f91a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 53
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "93958e31-8ddf-4be2-ab35-936d339a79fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "fd6c58a6-4ffd-49c7-9abe-16caa0f9a9bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 242,
                "y": 36
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "66e26da2-e5e0-4d72-b8b8-47e24875139b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 232,
                "y": 36
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "fb4e02fe-31b3-4082-8621-5cf0618d4921",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 220,
                "y": 36
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "dc832869-2915-4378-a18f-75ff33c07956",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 209,
                "y": 36
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5a72eda3-2ec4-406f-9e7e-9298b133b6cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 204,
                "y": 36
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "84c2421d-8f14-411d-abf6-7e4f92b669a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 195,
                "y": 36
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5b5f1ebd-778e-44f6-8420-dd51ddec89aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 183,
                "y": 36
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "25904e02-df60-4a5a-b550-ac77080f12b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 174,
                "y": 36
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "332696ed-991c-46e5-9e8c-d4ae1fe33a91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 162,
                "y": 36
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "bd57af25-5a87-431c-8880-f9e4352e4601",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 202,
                "y": 19
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "9532d9cd-6959-4f03-ae57-8a4c71340f41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 190,
                "y": 19
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "9d28bf3e-21f6-42ad-a1e4-9f5109517b78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 179,
                "y": 19
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "04e6b029-b518-4c18-8e4e-85253f4c26d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d4f9d2c0-3af6-4c56-85a7-2e95ef543215",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "c2eff369-ce30-401a-8dbb-abd0a3d7dbde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "47c5e8f8-5b30-45df-9714-f83723e57cb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "25ffdd49-10ba-40fa-b3f4-5743f22554c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "871bfa6e-2825-40c5-8f43-7c2c88e639d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c06c7581-c815-4f93-b53f-8a6df245d38f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "0c77b2d6-befd-4bdd-b8bd-37e4ae490cac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "92cc0b6d-9cbb-4b79-9957-bb51a6d99419",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 15,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "356c19a0-8445-4ec6-87e1-3370d4662a20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "624c1f28-511b-4098-95fd-24862ffab60a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d5765482-3f46-48b0-a0f2-6235d6db1e00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "826b6e69-9b81-4d1d-b1c2-3e43ff34d83e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "471dbae3-a619-4fa6-9f6f-72f2a751d488",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "52d029d6-5849-4550-adca-36920f9b5055",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 15,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "aa7c6d5c-8c9c-4e55-9f0d-04c12ff33171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d09e6b95-3e52-418f-86db-3e14003f8240",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "6db74e84-096b-45ed-b03f-23f754627a11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "c7c72f49-3cd3-468a-9626-c47848e3be36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "79be0f4c-1b77-4c2e-966d-4d581d8b5f7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "78b9864d-73ef-47ec-9221-90ff366ea751",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "043f12ec-af00-47be-a5b7-f8b9974acde1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "56dc0eac-8615-4150-9fd4-5fb1b071695c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "32b5ad75-df61-4b90-be78-a4f1a4b42d90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 71,
                "y": 19
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "802e2d29-092a-41d3-b83b-fc53e82db651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "bde5de6e-c0b2-4090-8adc-2c5f2bc33be9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 164,
                "y": 19
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "7ce52c34-429a-4d88-9a2e-7fc2429fce36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 154,
                "y": 19
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "75373465-d8ca-42fc-90e3-2f457308e1c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 149,
                "y": 19
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a3a94360-61ad-4b13-a7d1-ae02c2b73b67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 136,
                "y": 19
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "72bc2cd8-d7fc-45b4-a716-8dde0225be54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 126,
                "y": 19
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "38769efa-5c54-4403-800f-3ee8603f8e79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 116,
                "y": 19
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "3b36f963-5271-4dbf-a3bd-e351d018c0b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 106,
                "y": 19
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ac318991-9367-4547-9947-76f05fda084a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 96,
                "y": 19
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e686601e-8f95-45d4-ab52-ebd6928906c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 88,
                "y": 19
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9c5f78ab-c807-4b65-9f5c-e1ffbdf10cc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 170,
                "y": 19
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "307a8677-fb49-4a02-a145-143c06d4808e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 81,
                "y": 19
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "663dfa20-a0a6-4e33-8588-e6c346dc291a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 61,
                "y": 19
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "8f51c830-3542-498a-9ddc-35a841d2f6f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 51,
                "y": 19
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "b834e43f-d97f-4174-8314-285d03974881",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 38,
                "y": 19
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "7a852c86-4338-43fe-adf1-8129c0475800",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 28,
                "y": 19
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "908d0471-8874-437d-bb6e-a21a1a16d61f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 18,
                "y": 19
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1c6cd96a-7fde-4527-bc74-b8ae7bee7289",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 9,
                "y": 19
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "eb2d70c9-8522-4f55-94ef-676ffb0ac43b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 19
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "bc35015b-4675-4260-bab1-bf752b6dfd93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 245,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1456f227-fa75-409d-b19f-0d794ca35f79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "5559af83-e986-4bd5-99bf-a84a35cc6666",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 119,
                "y": 53
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "9f8d6b77-cfd7-4d36-9e36-3b7f267bfb5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 15,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 129,
                "y": 53
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "c5e1532f-73bc-45f6-865a-fd41505bec97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "3ba66b7d-1287-4024-a241-c6010ff23930",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "1b6f81b7-7b00-46ce-8c28-1408dfbf4fbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "494266e4-7078-4dbc-a47b-454b0884aa95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "d85017a0-e9ce-42f0-827b-ebe4eb132ef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "4eb63ab0-3c60-443e-8595-f555327b1873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "cb35c40b-87c0-4ed7-a487-857b941d628d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "e5b78cfd-f0ae-45c9-8d3e-570886519be8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "7631f379-fd70-4c0b-bc37-2e8edfe65b0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "e726529e-f431-45be-a3cf-23c5e71a4557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "fda3ad53-f3c7-4b88-9dca-f656400ecd30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "e31b51ec-c22d-48f3-be6b-d29c29fd8f4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "4c5ef011-9f73-4483-b1b3-a5f67e4b061b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "ba721898-c5e8-4b8c-bd9b-3ac0a7adb9e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "28cb182d-14e4-4382-a8b5-e263de2020c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "908a703f-37d4-42b8-8f3f-038073bbb20c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "19517df4-40e7-47cb-ad60-634c28886950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "03bba3a1-ae31-4448-b900-337bf5aeff16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "1c21bf6e-b510-4384-b5cb-6d098d17f56c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "7a416e6b-26c9-42fa-a419-b13bc9824117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "87ed4f33-195e-4cd0-8f7a-c43e79d4b445",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "21348c74-075a-49d4-b337-136eb97f4b43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "837bba87-1361-48f0-b1de-8fe13c37ea3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "0ee1d838-7d74-413e-95f2-26cb9e5c7ee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "d7ac9071-1e2a-45a3-a0c8-f45762801144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "c0990d83-3c32-4046-b368-35f4c666c161",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "25fcc7f6-c37a-4591-8f2c-d76d44d04461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "f68c6cb5-3b38-4f27-9175-babeb6024550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "5f670b59-77ce-4bb5-97c5-00e75cd8a886",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "375190a0-c18c-4070-90b1-42ce1f81c34c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "b9ab94e8-bde6-421c-b399-9a416b19f806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "dc8beada-87a6-476d-9d4a-528c57148bfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "7890ca70-6068-4e79-8edb-a87a47d7be53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "c3cdff7f-4383-4b35-b37e-bed2342721ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "49711e23-86f6-4642-afdf-fc16739e6bb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "356431a2-6cae-4234-808c-6372ec5acebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "9fb95ffc-d939-42bc-9951-a86ade6f6805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "f0838e47-c731-47e1-beb5-24f342ec2aa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "21805f84-04fc-4696-9b08-01d1edd1d3d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "e5f374f1-1575-4fe0-a1a3-ae512673b823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "d2463225-41ab-47cc-a640-0672d15942a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 10,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}