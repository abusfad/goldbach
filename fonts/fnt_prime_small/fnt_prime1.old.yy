{
    "id": "28a15973-a29a-42ea-83e4-b7bb6bb9a0fc",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_prime",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "99ebdcd3-86ee-410f-a3f5-8c0c4fd066d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "cef6f132-f8b9-4063-9d03-2969943c7c73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 123,
                "y": 36
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "782c03db-7c8e-402a-9faa-3ebda8442f26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 115,
                "y": 36
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "aefe1f19-6258-4641-957b-364902b2de3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 105,
                "y": 36
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "96c167c7-8954-4eca-9043-d8d8c9a1023c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 96,
                "y": 36
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5e11fb6b-812f-4893-bf61-b56a69060ac1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 83,
                "y": 36
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "235fa6cc-7665-4dab-9022-bddf10c005ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 71,
                "y": 36
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c0585999-7a04-420a-b08b-5be649156e10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 66,
                "y": 36
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "61a0d2c6-ce3b-438b-82ee-cb00566a29a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 60,
                "y": 36
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "7f451313-77df-4231-9d7b-ce31db7d3f8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 54,
                "y": 36
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c1f99e02-ac37-41c5-a5eb-e2d906539417",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 128,
                "y": 36
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "78867736-cbba-4ab3-9417-52bdcf3ba405",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 44,
                "y": 36
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b5f9cda5-dab0-4e35-b8ec-795aea9cae7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 30,
                "y": 36
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "d217c40f-8385-4cd4-a47f-e5f1d6c5e702",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 23,
                "y": 36
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ee8f7711-470e-4685-b505-4bb283dd0525",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 18,
                "y": 36
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "2a7fe5fb-41b1-4ae6-a612-0ced72a7319c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 11,
                "y": 36
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "fdf4555a-c06e-4ecd-bb52-15a422dfc6b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a0e1509d-6634-4c5b-8f65-ee6e5b3cf4ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 240,
                "y": 19
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "5fda899c-9505-4b82-b311-4360ca16e1a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 231,
                "y": 19
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "744be53f-2d5c-46f5-b072-fff5d92a0e58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 222,
                "y": 19
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "2f4ea9c8-fbc3-4ca9-af84-7c135590542c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 213,
                "y": 19
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "920620ed-47cf-48a4-87d4-4e4be71e6485",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 35,
                "y": 36
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "0a03faf9-1875-4170-a5a9-65356eb43cd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 135,
                "y": 36
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "26b893bc-73ce-44b4-a521-494410e730be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 144,
                "y": 36
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a5df96ae-9c74-46c6-aeec-92488197c01a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 153,
                "y": 36
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "4754ddb4-0365-433c-b7d8-73f27be046a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 110,
                "y": 53
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "5f1933ab-5684-4f1f-bc04-75f86edb83c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 105,
                "y": 53
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "96c86950-d3dc-4272-a0b2-1794e017e294",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 100,
                "y": 53
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "70f8e71f-fa6f-40a3-a3ac-ccbf681d30c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 91,
                "y": 53
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "50f46dce-423e-4c89-9183-873c1471cc84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 81,
                "y": 53
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "44aa0550-94ca-42d2-936e-86c4d7e7957a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 72,
                "y": 53
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c6e80930-08a8-441a-9139-0f364d96f834",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 62,
                "y": 53
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "24f04154-eac5-4944-a898-7ff00a697de8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 15,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 47,
                "y": 53
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "59c8ad4f-2f04-426f-a91f-5a133a1677d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 35,
                "y": 53
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "d08fdd9c-7b32-43f7-85cd-4240922304de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 53
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "82dec084-d3e3-4618-b445-670f1d8df56b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 53
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "371ffedc-2277-4f8f-9dfe-d1ced85195a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "fa6843e3-545f-4402-afbf-4f9b4bbdae13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 242,
                "y": 36
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e5d1aa71-452f-4508-a5a9-71ebd4e210fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 232,
                "y": 36
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1d3b8243-03e2-4f5b-810b-45a340e49e45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 220,
                "y": 36
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "ceb3476d-702b-4355-969c-5af1fc7fcfb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 209,
                "y": 36
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "4a6f9ea8-37d4-46a6-992a-d8e034992c46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 204,
                "y": 36
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ef52a30e-26f8-434b-a715-fd3f02c0f680",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 195,
                "y": 36
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9d44d3db-16e9-402a-a7a1-c50a174ff98d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 183,
                "y": 36
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c7e80fc1-7375-4c44-9700-7963e273069e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 174,
                "y": 36
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b65010c8-5e60-4518-9f79-1749ebae5154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 162,
                "y": 36
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "1839220a-9b82-4577-a3cb-503710a25edc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 202,
                "y": 19
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "899e80c8-e5f6-48d7-b99b-4415195624f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 190,
                "y": 19
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "6617ff37-0158-4418-bdc6-c249cf710301",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 179,
                "y": 19
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e8296845-1cc1-49c5-8e9d-a9199a7e2eea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "0a774188-681a-4b5a-b012-d9a3de9afb52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8ccd9c4c-93cc-40fc-94aa-a769d60e59a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "967aa775-cc42-4caa-9cf7-1ee797a8e276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "4cea8429-6d68-4c33-952e-d92a5ccf03d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "af139086-2732-499c-a253-aea3f80acda7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "763a5825-2b19-4292-9b31-6f343f257c4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "cd6a5daf-cf34-4033-947e-0a1f1b7fbf5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "10227683-a662-4418-bf85-35b0a2da9c24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 15,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "22f7c5e9-a436-4b32-9be6-aabb971fa9d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "39974778-60e3-47cf-b8fc-4eab8e697807",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "da6838f6-41de-4b94-9a8a-29e3c4cdf50c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "2ee31344-2ae0-4f7d-aeb2-bdb01edc4625",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6d8995bf-3ef5-4fc1-8f31-ddec2c18fcfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "db890775-1f20-4e70-b3a2-bce30ae84eda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 15,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "6dfab23f-5536-4aee-aa9a-1d066da25ab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "0830d382-da83-4473-a7fc-ca755ce99db7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5a14e61b-5622-4e00-bdc0-2c42ab78975e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "54c7025c-a121-454a-ba9e-cc58a0d1983d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e411a67e-f443-4dc2-9f88-1ab7fb0c90bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "c75270c0-41e2-44f5-bd51-b117cf1cc27f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "4f8f56a6-32fc-4ccf-b516-be83db11f893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "134e859c-40ba-4efb-a52c-92a20712afea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "99bab5f6-9d30-47d5-b078-105a4ec86b0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 71,
                "y": 19
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "b498868a-42d8-4ab4-bb91-b4ea2ee11bf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "17b98835-e853-4ef3-b27b-713d3b84a0cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 164,
                "y": 19
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "06ebe45e-cbde-4ec7-993b-5ae20f655874",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 154,
                "y": 19
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c1e0f146-be67-4b4d-ab6e-23ecf513bdbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 149,
                "y": 19
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c05dfd57-e173-4781-87f3-a0603fadfc86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 136,
                "y": 19
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "06616dfa-2c46-4262-a15c-e46c5722d8e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 126,
                "y": 19
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c5024575-e9c0-4389-a603-f6993e4575d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 116,
                "y": 19
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "4bf2f468-76a9-479a-9c7d-b5a759d9b545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 106,
                "y": 19
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "9b917be3-6f55-47a7-85a2-95b7d1b6da33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 96,
                "y": 19
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "5af52574-f023-40cb-bc12-15e6b0b3d08d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 88,
                "y": 19
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9bd5c026-024b-444b-b1ef-e025a38703fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 170,
                "y": 19
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a8609a61-8768-4448-8931-02b3ab562c16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 81,
                "y": 19
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "44ed67fb-4fe5-401c-84d4-a7576e617c21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 61,
                "y": 19
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c07d9932-85b0-467f-a33c-31dd9896e4f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 51,
                "y": 19
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "71a2df14-a588-4874-9234-e6f4055c3adc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 38,
                "y": 19
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8c343514-8ef7-41ad-aab1-6bccf90c8db5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 28,
                "y": 19
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "9fec08d3-0c37-4c3d-8131-a1ea74b911b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 18,
                "y": 19
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d05ab975-69c7-46f5-a27b-1f19df8b0d23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 9,
                "y": 19
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c01e1bc4-17ee-4dcb-ba32-60e394ec19be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 19
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "48f86730-5053-4120-b582-06491687c6fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 245,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "74aaf16c-9eec-4d21-b5c5-b2668cda3d20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "ff18407a-3fdc-4ecd-9aa9-aa735e201bd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 119,
                "y": 53
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "539f64ab-ecf2-4af4-b74b-e6f1cefbc0ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 15,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 129,
                "y": 53
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "09e80cb9-f869-417e-aa9f-521f0994a837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "4f371f8e-6ec8-4728-b031-fd85f50dd596",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "2f697d5b-57da-431a-a1c5-1457d297f25e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "694117d0-c33e-485f-b9a3-ff42cc67f21f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "f1522cc6-a697-40f0-8278-7888e9a5e7c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "881073ab-9de5-4347-a352-886220137064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "4c2ca3be-e468-4fbf-8207-19536982b237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "527dafbc-09a5-4e81-86f6-eec543f594dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "99ed10af-4530-44e7-a2dd-9aa8e565bf37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "bc759545-86b3-4732-a6a9-b2393ef45053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "79602d26-84e3-4c08-a197-276949796a22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "b837e210-9499-498d-a39c-eab87c26a583",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "4d6c8d03-d4b0-47b0-bb8b-da6c1bce1040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "79500bc6-7705-4017-8a79-af070d44f57a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "5d380e2f-3dbf-43a2-b1d2-debc2820f252",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "4087aa49-0280-41e4-8800-c7dfcb263bd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "abdf3b9b-9662-4fad-a31e-643f36a6280c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "b934dba8-78d6-474d-ad54-05894735d09d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "acabdaad-3637-4383-bb2b-b73a203bc4ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "e3d78bd1-5596-456a-a196-400ec5b3bfaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "f391559c-ca0a-4c2c-a484-580585dbad5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "f1b903b9-ef16-4447-82b0-be0bcd0993fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "4d1c5214-46f2-4bb3-854c-7c5f8c4793b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "e78144e6-a42a-4c00-a442-b711591eb8da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "805dd150-479c-402b-9db8-30c0c7bd48a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "aee9e825-7c16-4039-b4cc-99d4779c91ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "fe9e5a0a-8387-447a-a88f-7df7902cd59f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "3b022dc6-b3b1-4eaf-81b8-c97e37270ab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "e23664ae-f841-4575-b4c1-b1bab2d3b9a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "888a6abc-5c1c-4c15-b43c-b22ebe41093d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "5ea0628a-d56a-4a4a-ad1c-9102c6f867a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "5a496e18-e4a7-4ee0-82c1-441a682dc06f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "87e16ad6-88a5-41b9-b188-325111b19748",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "75f5c629-7f17-42d6-9861-9839d816d5bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "6a983644-6e3a-4629-989d-0e757447770b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "3fd64109-21be-4d8a-9aef-b0d18103740e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "21fc6606-0323-491b-8042-a077d1909309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "1ecfb998-e20b-44b0-aa77-43e2d7487a8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "411f667b-b48f-46f8-addf-113571aa3723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "6dcb7b70-d0b7-469c-ba30-c881bfd36731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "1472b32c-f40a-40f3-b3d7-5d65001d460d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 10,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}