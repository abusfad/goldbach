//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
uniform float u_time;
uniform float u_alpha;

void main()
{
	vec2 coord_front = vec2(v_vTexcoord.x - u_time, v_vTexcoord.y);
	vec2 coord_back = vec2(1.0 - (v_vTexcoord.x + u_time), v_vTexcoord.y);
    vec4 col_front = texture2D( gm_BaseTexture, coord_front );
    vec4 col_back = texture2D( gm_BaseTexture, coord_back );
	vec4 col = mix(col_front, col_back, step(col_back.r, col_front.r));
	col.a = u_alpha;
	gl_FragColor = col;
}
