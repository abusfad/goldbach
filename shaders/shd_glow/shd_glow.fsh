//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
uniform vec4 u_glow_color;
uniform sampler2D smp_glow;

float LumOf (vec3 col)
{
    return dot(col, vec3(0.299, 0.587, 0.14));
}

void main()
{
	vec4 col = texture2D( gm_BaseTexture, v_vTexcoord );
	vec4 glow = texture2D( smp_glow, v_vTexcoord );
	glow.a = LumOf(glow.rgb);
	glow *= u_glow_color;
	
    gl_FragColor = mix(col, glow, step(col.a, 0.7));
}
