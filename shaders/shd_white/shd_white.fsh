//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;

void main()
{
	vec4 col = texture2D( gm_BaseTexture, v_vTexcoord );
	//gl_FragColor = vec4(1.0);
	gl_FragColor = vec4(col.a);
}
