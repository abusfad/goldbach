varying vec2 v_vTexcoord;

uniform vec2 u_blur_vector; //should be (1,0) for horizontal blur or (0,1) for vertical
uniform int u_blur_length;
uniform float u_blur_kernel[32];
uniform vec2 u_texel;

void main()
{
	vec4 col = texture2D( gm_BaseTexture, v_vTexcoord);
	float weight_sum = u_blur_kernel[0];
	for (int i=1; i<=u_blur_length; i++)
	{
		col += texture2D( gm_BaseTexture, v_vTexcoord+float(i)*u_texel*u_blur_vector)*u_blur_kernel[i];
		col += texture2D( gm_BaseTexture, v_vTexcoord-float(i)*u_texel*u_blur_vector)*u_blur_kernel[i];
		weight_sum += 2.0*u_blur_kernel[i];
	}
	gl_FragColor = col / weight_sum;
}