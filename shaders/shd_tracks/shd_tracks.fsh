//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
uniform float u_time;
uniform float u_alpha;

void main()
{
	vec2 coord = vec2(v_vTexcoord.x - u_time, v_vTexcoord.y);
    vec4 col = texture2D( gm_BaseTexture, coord );
	col.a = u_alpha;
	gl_FragColor = col;
}
